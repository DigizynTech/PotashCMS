<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalProvider extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function MedicalCategory()
    {
        return $this->belongsTo(MedicalProviderCategory::class, 'category', 'id');
    }

    public function subCategory()
    {
        if ($this->category != 0) {
            return $this->MedicalCategory->parentCatRelation;
        } else {
            return null;
        }
    }

    public function MajorMedicalField(): int {
        if ($this->MedicalCategory->parent == 0) {
            return $this->category;
        } else {
            return $this->MedicalCategory->parent;
        }
    }
}
