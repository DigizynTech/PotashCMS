<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class News extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'publish_datetime' => 'date'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('thumb')
            ->singleFile()
            ->useFallbackUrl(asset('images/default-image.png'));

        $this->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl(asset('images/default-image.png'));

    }
}
