<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MedicalProviderCategory extends Model
{

    use HasFactory;

    protected $guarded = ['id'];

    public function parentCat()
    {
        return MedicalProviderCategory::where('id', $this->parent)->first()->name ?? '';
    }

    public function parentCatRelation(): BelongsTo
    {
        return $this->belongsTo(MedicalProviderCategory::class,'parent','id');
    }
}
