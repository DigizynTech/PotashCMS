<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdvanceRequestMail extends Mailable
{
    use Queueable, SerializesModels;
    private User $user;
    private $amount;
    private $period;
    private $iban;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $amount, $period, $iban)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->period = $period;
        $this->iban = $iban;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.advance', [
            'user' => $this->user,
            'amount' => $this->amount,
            'period' => $this->period,
            'iban' => $this->iban
        ]);
    }
}
