<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DayLeaveMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $jobid;
    private $info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $jobid, $info)
    {
        $this->user = $user;
        $this->jobid = $jobid;
        $this->info = $info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.dayleave', [
            'user' => $this->user,
            'jobid' => $this->jobid,
            'info' => $this->info
        ]);
    }
}
