<?php

namespace App\Mail;

use App\Models\Ideas;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendIdea extends Mailable
{
    use Queueable, SerializesModels;

    private Ideas $idea;
    private User $user;
    private string $categories;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ideas $idea, User $user)
    {
        $this->idea = $idea;
        $this->user = $user;
        $categories = [
            1 => 'العمليات ورفع الكفاءة والقدرة الإنتاجية',
            2 => 'بيئة العمل',
            3 => 'رفع جودة المنتج',
            4 => 'كفاءة وترشيد استهلاك الطاقة والمياه',
            5 => 'خفض الكلف والنفقات',
            6 => 'أخرى',
        ];

        $idea_cats = explode(',', $idea->categories);

        $cs = [];
        foreach ($idea_cats as $cat) {
            try {
                $cs[] = $categories[$cat];
            } catch (\Exception $e) {
                $cs[] = '--';
            }
        }

        $this->categories = implode(',', $cs);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.idea',
            [
                'idea' => $this->idea,
                'user' => $this->user,
                'categories' => $this->categories,
            ]);
    }
}
