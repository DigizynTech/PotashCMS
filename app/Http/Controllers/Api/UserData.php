<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class UserData extends Controller
{
    public function userData(): JsonResponse
    {
        $funds = [
            '2017' => 103,
            '2018' => 107,
            '2019' => 150,
            '2020' => 180,
            '2021' => 201,
        ];

        $medical_cuts = 190;
        $number_of_visits = 3;
        $full_leaves_count = 7;
        $saving_funds_balance = 201;

        return Response::json([
            'status' => 200,
            'response' => [
                'name' => 'رامي سمير عز الدين ملكاوي',
                'Job title' => 'IT Director',
                'Level' => 'IT Manager',
                'Direct supervisor' => "CEO",
                'Department' => 'IT',
                'Job_id_number' => '6202',
                'Mobile' => '0774521452',
                'funds' => $funds,
                'medical_cuts' => $medical_cuts,
                'number_of_visits' => $number_of_visits,
                'full_leaves_count' => $full_leaves_count,
                'saving_funds_balance' => $saving_funds_balance]
        ]);
    }
}
