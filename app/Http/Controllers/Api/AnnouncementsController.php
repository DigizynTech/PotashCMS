<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Response;

class AnnouncementsController extends Controller
{
    public function all(Request $request): JsonResponse
    {
        $announcements = Announcement::where('status','published')->orderByDesc('created_at')->get();

        $response = [
            'status' => 200,
            'response' => ['announcements' => []]
        ];
        foreach ($announcements as $announcement) {
            $link = $announcement->getFirstMediaUrl('attachments');
            $response['response']['announcements'][] = [
                'ID' => $announcement->id,
                'Title' => $announcement->title,
                'Content' => strip_tags($announcement->content),
                'Date' => $announcement->published_at->format('Y-m-d'),
                'attachment' => (Str::length($link) > 4) ? $link : $announcement->attachment
            ];
        }
        return Response::json($response);
    }

    public function show(Request $request, $id): JsonResponse
    {
        $announcement = Announcement::where('id', $id)->first();
        if ($announcement) {
            $message = [
                'status' => 200,
                'response' => [
                    'ID' => $announcement->id,
                    'Title' => $announcement->title,
                    'Content' => $announcement->content,
                    'Date' => $announcement->published_at->format('Y-m-d'),
                    'attachment' => $announcement->getFirstMediaUrl('attachments')
                ]
            ];
        } else {
            $message = [
                'status' => 404,
                'message' => 'Object not found'
            ];
        }

        return Response::json($message);
    }
}
