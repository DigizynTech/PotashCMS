<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\AdvanceRequestMail;
use App\Mail\DayLeaveMail;
use App\Mail\WorkLeaveMail;
use App\Models\AdvanceRequest;
use App\Models\AdvanceRequestOrder;
use App\Models\User;
use App\Models\UserLeave;
use Auth;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function auth(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'job_id_number' => 'required',
            'password' => 'required',
            'device_name' => 'nullable',
            'device_token' => 'nullable',
        ]);
        if ($validator->fails()) {
            $message = [
                'status' => 400,
                'response' => $validator->errors()
            ];
            return Response::json($message);
        }

        $user = User::where('job_id_number', $request->get('job_id_number'))->first();
        if (!$user || !Hash::check($request->get('password'), $user->password)) {
            $message = [
                'status' => 401,
                'response' => ['token' => null]
            ];
        } else {
            if ($user->accepted) {
                $message = [
                    'status' => 200,
                    'response' => ['token' => $user->createToken($request->get('device_name'))->plainTextToken]];
                $user->update([
                    'device_token' => $request->get('device_token'),
                    'device_name' => $request->get('device_name'),
                ]);

            } else {
                $message = [
                    'status' => 402,
                    'response' => ['token' => null]
                ];
            }
        }

        return Response::json($message);
    }

    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'job_id_number' => 'required',
            'name' => 'required',
            'mobile' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $message = [
                'status' => 400,
                'response' => $validator->errors()
            ];
            return Response::json($message);
        }

        $user = User::where('job_id_number', $request->get('job_id_number'))->first();
        if ($user) {
            $message = [
                'status' => 401,
                'response' => ['token' => null]
            ];
        } else {
            $user = User::create([
                'email' => $request->get('job_id_number') . '@arabpotash.com',
                'job_id_number' => $request->get('job_id_number'),
                'name' => $request->get('name'),
                'mobile_number' => $request->get('mobile'),
                'password' => Hash::make($request->get('password'))
            ]);
            $device_name = $request->get('device_name') ?? 'mobile';
            $potash = new PotashDB($request->get('job_id_number'));
            $e_info = $potash->getEmployeeInfo();
            if ($e_info['organization_name']) {
                $user->update([
                    'department' => $e_info['organization_name'],
                    'job_title' => $e_info['job_name'],
                    'fullname' => $e_info['emp_name_arb']
                ]);
            }
            $message = [
                'status' => 200,
                'response' => ['token' => $user->createToken($device_name)->plainTextToken]];
        }
        return Response::json($message);
    }

    public function profile(): JsonResponse
    {
        $user = Auth::user();
        $PotashDB = new PotashDB($user->job_id_number);
        $emp_info = $PotashDB->getEmployeeInfo();
        if ($emp_info == false) {
            $message = [
                'status' => 200,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }
        $filteredUserData = [];
        $advance_request = AdvanceRequest::orderByDesc('date')->where('user_id', $user->job_id_number)->first();
        $medical_info = $PotashDB->getMedicalInfo();

        if ($advance_request) {
            $savings = $advance_request->saving_account_balance;
        } else {
            $savings = 0;
        }
        $saving_fund_history = AdvanceRequest::where('user_id', $user->job_id_number)->orderByDesc('date')->get();
        foreach ($saving_fund_history as $single_row) {
            $filteredUserData[] = [
                'date' => Carbon::parse($single_row->date)->format('M-Y'),
                'balance' => $single_row->saving_account_balance
            ];
        }
        if (count($filteredUserData) == 0) {
            $filteredUserData[] = [
                'date' => null,
                'balance' => null
            ];
        }
        $data = [
            'name' => $emp_info['emp_name_eng'],
            'job_id_number' => $user->job_id_number,
            'job_title' => $emp_info['job_name'],
            'mobile' => $user->mobile_number,
            'department' => $emp_info['organization_name'],
            'vacation_balance' => $PotashDB->getVacationBalance(),
            'medical_cuts' => $medical_info['MedicalDeductions'] ?? 0,
            'medical_visits' => $medical_info['MedicalVisits'] ?? 0,
            'saving_fund_balance' => $savings,
            'statistics' => $filteredUserData
        ];

        $message = [
            'status' => 200,
            'response' => $data
        ];
        return Response::json($message);

    }

    public function advance_request(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'period' => 'required',
            'iban' => 'nullable'
        ]);
        if ($validator->fails()) {
            $message = [
                'status' => 400,
                'response' => $validator->errors()
            ];
            return Response::json($message);
        }
        AdvanceRequestOrder::create([
            'user_id' => Auth::user()->id,
            'request_amount' => $request->get('amount'),
            'payment_period' => $request->get('period'),
            'iban' => $request->get('iban'),
        ]);

        $message = [
            'status' => 200,
            'response' => 'Advance request added successfully'
        ];

        $user = Auth::user();
        Mail::to('omar.a@arabpotash.com')
            ->send(new AdvanceRequestMail($user,  $request->get('amount'), $request->get('period'), $request->get('iban')));
        return Response::json($message);
    }

    public function dayLeave(Request $request)
    {

        $user_id = Auth::user()->id;
        UserLeave::create([
            'user_id' => $user_id,
            'leave_type' => 'day',
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'date_day_leave' => $request->get('date'),
            'reason' => $request->get('reason'),
            'accepted' => false,
        ]);

        $info = [
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'date_day_leave' => $request->get('date'),
            'reason' => $request->get('reason'),
        ];
        Mail::to(env('MAIL_TO'))
            ->send(new DayLeaveMail(Auth::user()->name, Auth::user()->job_id_number, $info));
        $message = [
            'status' => 200,
            'response' => 'Work Leave added successfully'
        ];
        return Response::json($message);
    }

    public function workLeave(Request $request): JsonResponse
    {

        $user_id = Auth::user()->id;

        UserLeave::create([
            'user_id' => $user_id,
            'leave_type' => 'work',
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'accepted' => false,
        ]);

        $info = [
            'leave_type' => 'work',
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'noDays' => $request->get('noDays'),
        ];
        Mail::to(env('MAIL_TO'))
            ->send(new WorkLeaveMail(Auth::user()->name, Auth::user()->job_id_number, $info));
        $message = [
            'status' => 200,
            'response' => 'Work Leave added successfully'
        ];
        return Response::json($message);
    }

    public function attendance(): JsonResponse
    {
        $user = Auth::user();
        $PotashDB = new PotashDB($user->job_id_number);
        $attendance = $PotashDB->getEmployeeAttendance();
        if ($attendance === false) {
            $message = [
                'status' => 200,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }

        $data = ['Attendance' => []];
        foreach ($attendance as $day) {
            try {
                $date = Carbon::parse($day['da_date'])->format('Y-m-d');
            } catch (InvalidFormatException $error) {
                $date = $day['da_date'];
            }
            $data['Attendance'][] = [
                'date' => $date,
                'from' => $this->formatTimeLessThan10($day['st_time_hh']) . ":" . $this->formatTimeLessThan10($day['st_time_mi']),
                'to' => $this->formatTimeLessThan10($day['en_time_hh']) . ":" . $this->formatTimeLessThan10($day['en_time_mi']),
                'type' => $day['transaction_type']
            ];
        }

        $message = [
            'status' => 200,
            'response' => $data
        ];
        return Response::json($message);
    }

    public function getDayLeaves(): JsonResponse
    {
        if (!Auth::check()) {
            $message = [
                'status' => 401,
                'response' => 'Unauthorized'
            ];
            return Response::json($message);
        }

        $PotashDB = new PotashDB(Auth::user()->job_id_number);
        $day_leaves = $PotashDB->getEmployeeLeaves();
        if ($day_leaves === false) {
            $message = [
                'status' => 200,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }
        $data = ['leaves' => []];
        foreach ($day_leaves as $day) {
            try {
                $date = Carbon::parse($day['date_from'])->format('Y-m-d');
            } catch (InvalidFormatException $error) {
                $date = $day['date_from'];
            }

            $total_hours = $day['total_hour'];
            if ($total_hours == 0.5) {
                $total_hours = "0.5";
            }

            $data['leaves'][] = [
                'date' => $date,
                'from' => '',
                'to' => '',
                'type' => $day['sub_tran_type'],
                'Interval' => " " . $total_hours . " "
            ];
        }

        $leaves = collect($data['leaves']);
        $ordered = $leaves->sortByDesc(function ($vacation) {
            return Carbon::parse($vacation['date'] . ' ' . $vacation['from'])->timestamp;
        });
        $data['leaves'] = $ordered->values();

        $message = [
            'status' => 200,
            'response' => $data
        ];
        return Response::json($message);
    }

    public function getWorkLeaves(): JsonResponse
    {
        if (!Auth::check()) {
            $message = [
                'status' => 401,
                'response' => 'Unauthorized'
            ];
            return Response::json($message);
        }

        $PotashDB = new PotashDB(Auth::user()->job_id_number);
        $vacations = $PotashDB->getVacationHistory();
        if ($vacations === false) {
            $message = [
                'status' => 201,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }
        $data = ['vacations' => []];
        foreach ($vacations as $vacation) {
            $interval = Carbon::parse($vacation['abs_date_from'])->diffInDays(Carbon::parse($vacation['abs_date_to'])) + 1;
            $data['vacations'][] = [
                'from' => $vacation['abs_date_from'],
                'to' => $vacation['abs_date_to'],
                'type' => $vacation['absence_type'] . ' - ' . $vacation['sick_reason'],
                'Interval' => $interval,
            ];
        }

        $vacations = collect($data['vacations']);
        $ordered = $vacations->sortByDesc(function ($vacation) {
            return Carbon::parse($vacation['from'])->timestamp;
        });
        $data['vacations'] = $ordered->values();

        $message = [
            'status' => 200,
            'response' => $data
        ];
        return Response::json($message);
    }

    public function getAdvanceRequest(): JsonResponse
    {
        $advance_request = AdvanceRequest::where('user_id', Auth::user()->job_id_number)
            ->orderByDesc('date')->first();

        if ($advance_request) {
            if ($advance_request->request_amount == 0 || $advance_request->request_amount == null) {
                $message = [
                    'status' => 201,
                    'response' => [
                        'amount' => null,
                        'remaining' => null,
                        'payment_period' => null,
                        'saving_fund_balance' => null,
                    ]
                ];
                $checker = AdvanceRequestOrder::where('user_id', Auth::user()->id)->where('created_at', ">", Carbon::now()->subDay(7))->first();
                if ($checker) {
                    $message['status'] = 202;
                }
            } else {
                $message = [
                    'status' => 200,
                    'response' => [
                        'amount' => $advance_request->request_amount,
                        'remaining' => $advance_request->remaining_amount,
                        'payment_period' => $advance_request->payment_period,
                        'saving_fund_balance' => $advance_request->saving_account_balance,
                    ]
                ];
            }
        } else {
            $message = [
                'status' => 201,
                'response' => [
                    'amount' => null,
                    'remaining' => null,
                    'payment_period' => null,
                    'saving_fund_balance' => null,
                ]
            ];
            $checker = AdvanceRequestOrder::where('user_id', Auth::user()->id)->where('created_at', ">", Carbon::now()->subDay(7))->first();
            if ($checker) {
                $message['status'] = 202;
            }
        }
        return Response::json($message);
    }

    public function getHouseLoan(Request $request): JsonResponse
    {
        if (!Auth::check()) {
            $message = [
                'status' => 401,
                'response' => 'Unauthorized'
            ];
            return Response::json($message);
        }

        $PotashDB = new PotashDB(Auth::user()->job_id_number);
        $HouseLoan = $PotashDB->getHouseLoans();
        if ($HouseLoan === false) {
            $message = [
                'status' => 200,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }

        $message = [
            'status' => 200,
            'response' => $HouseLoan
        ];

        return Response::json($message);

    }

    public function MedicalInfo(Request $request): JsonResponse {
        $user = Auth::user();
        $PotashDB = new PotashDB($user->job_id_number);

        $medicalInfo = $PotashDB->getMedicalInfo();

        if ($medicalInfo === false) {
            $message = [
                'status' => 200,
                'response' => "Oracle DB connection error"
            ];
            return Response::json($message);
        }

        $message = [
            'status' => 200,
            'response' => $medicalInfo
        ];
        
        return Response::json([$message]);
    }
    private function formatTimeLessThan10($time): string
    {
        $time = (int)$time;
        if ($time < 10) {
            return "0" . $time;
        } else {
            return $time;
        }
    }


}
