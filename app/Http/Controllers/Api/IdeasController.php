<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\SendIdea;
use App\Models\Ideas;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Mail;

class IdeasController extends Controller
{
    public function store(Request $request)
    {
        $validator = $request->validate([
            'title' => 'required',
            'categories' => 'nullable',
            'description' => 'nullable',
            'tools' => 'nullable',
            'steps' => 'nullable',
        ]);
        $user = Auth::user();

        $idea = Ideas::create([
            'title' => $validator['title'],
            'categories' => $validator['categories'],
            'description' => $validator['description'],
            'tools' => $validator['tools'],
            'steps' => $validator['steps'],
            'created_by' => $user->id,
        ]);

        try {
            Mail::to('innovative.ideas@arabpotash.com')->send(new SendIdea($idea, $user));
            $message = [
                'status' => 200,
                'message' => 'success'
            ];
            return Response::json($message);
        } catch (Exception $exception) {
            $message = [
                'status' => 200,
                'message' => 'success'
            ];
            return Response::json($message);
        }


    }
}
