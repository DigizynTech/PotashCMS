<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Http;

class PotashDB
{
    private string $url;
    private string $token;
    private string $employee_number;

    public function __construct($employee_number)
    {
        $this->url = "http://" . config('auth.DB_APP_IP');
        $this->token = config('auth.DB_APP_TOKEN');
        $this->employee_number = (string)$employee_number;
    }

    private function requestData(string $endpoint, string $employee_number): array|bool
    {
        $url = $this->url . '/api/v1/' . $endpoint;
        $request = Http::asJson()->withToken($this->token)->get($url, [
            'employee_id' => $employee_number
        ]);

        if ($request->status() == 200) {
            return $request->json()['response'];
        } else {
            return false;
        }

    }

    /**
     * @return int|array{employee_number: string,emp_name_arb: string,emp_name_eng: string,job_name: string,grade_name: string,organization_name:string,location_code: string}
     */
    public function getEmployeeInfo(): bool|array
    {
        $endpoint = 'EmployeeInfo';
        return $this->requestData($endpoint, $this->employee_number);
    }


    public function getEmployeeAttendance(): bool|array
    {
        $endpoint = 'EmployeeAttendance';
        return $this->requestData($endpoint, $this->employee_number)['attendance'];
    }

    public function getEmployeeLeaves(): bool|array
    {
        $endpoint = 'LeavesHistory';
        return $this->requestData($endpoint, $this->employee_number)['leaves'];
    }

    public function getVacationHistory(): bool|array
    {
        $endpoint = 'VacationHistory';
        return $this->requestData($endpoint, $this->employee_number)['vacations'];
    }

    public function getVacationBalance(): int
    {
        $endpoint = 'VacationBalance';
        return $this->requestData($endpoint, $this->employee_number)['employee_balance'];
    }

    /**
     * @return int|array{MedicalDeductions: string,MedicalVisits: string}
     */
    public function getMedicalInfo(): bool|array
    {
        $endpoint = 'MedicalInfo';
        return $this->requestData($endpoint, $this->employee_number);
    }

    public function getAllDepartments(): array {
        $endpoint = 'GetAllDepartments';
        return $this->requestData($endpoint,0)['departments'];
    }

    public function getHouseLoans(): bool|array {
        $endpoint = "HouseLoan";
        return $this->requestData($endpoint,$this->employee_number);
    }
}
