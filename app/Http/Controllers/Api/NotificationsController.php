<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class NotificationsController extends Controller
{
    public function all(Request $request): JsonResponse
    {
        $user = Auth::user();
        $user_id = $user->id ?? 0;
        $department = $user->department ?? '--';
        $news = News::where('departmentId', 'LIKE', 'All')
            ->orWhere(function ($query) use ($department) {
                $query->where('departmentId', 'LIKE', $department)->where('employeeId', 0);
            })->orWhere(function ($query) use ($department, $user_id) {
                $query->where('departmentId', 'LIKE', $department)->where('employeeId', $user_id);
            })->orderByDesc('created_at')->get();
        $response = [
            'status' => 200,
            'response' => ['notifications' => []]
        ];
        foreach ($news as $notification) {
            $response['response']['notifications'][] = [
                'ID' => $notification->id,
                'Title' => $notification->title,
                'Content' => strip_tags($notification->full),
                'Date' => $notification->created_at->format('Y-m-d'),
            ];
        }
        return Response::json($response);
    }
}
