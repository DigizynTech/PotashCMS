<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MedicalProvider;
use App\Models\MedicalProviderCategory;
use App\Models\Options;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class MedicalProviderController extends Controller
{
    public function getAllMedicalProviders(): JsonResponse
    {
        $md = MedicalProvider::where('id', '>', 0)->with('MedicalCategory')->orderByDesc('created_at')->get();

        $message = [
            'status' => 200,
            'response' => ['providers' => []],
        ];

        foreach ($md as $provider) {
            $message['response']['providers'][] = [
                'name' => $provider->name,
                'nameAr' => $provider->name_ar,
                'phone' => $provider->phone,
                'phone_2' => $provider->phone_2,
                'address' => $provider->address,
                'address_2' => $provider->address_ar,
                'category' => $provider->MajorMedicalField(),
                'sub_category' => ((int)$provider->category > 9) ? (int)$provider->category - 9 : $provider->category,
                'city' => $provider->city,
                'speciality' => $provider->MedicalCategory->name,
                'specialityAr' => $provider->MedicalCategory->name_ar,
            ];
        }
        return Response::json($message);
    }

    public function MedicalProviderVersion(): JsonResponse
    {
        $message = [
            'status' => 200,
            'response' => (int)Options::where('option_key', 'MedicalProvidersVersion')->first()->option_value,
        ];
        return Response::json($message);
    }

    public function MedicalProvidersCategories(): JsonResponse
    {
        $categoriesQuery = MedicalProviderCategory::where('parent', 0)->get();
        $categories = [];
        foreach ($categoriesQuery as $category) {
            $categories[] = [
                'id' => $category->id,
                'name' => $category->name,
                'nameAr' => $category->name_ar,
                'parent' => 0,
            ];
        }
        $message = [
            'status' => 200,
            'response' => [
                'categories' => $categories
            ],
        ];
        return Response::json($message);
    }

    public function MedicalProvidersSubCategories(): JsonResponse
    {
        $categoriesQuery = MedicalProviderCategory::where('parent', '!=', 0)->get();
        $categories = [];
        foreach ($categoriesQuery as $category) {
            $categories[] = [
                'id' => $category->id - 9,
                'name' => $category->name,
                'nameAr' => $category->name_ar,
                'parent' => $category->parent
            ];
        }
        $message = [
            'status' => 200,
            'response' => ['categories' => $categories],
        ];
        return Response::json($message);
    }

    public function getCityId(string $city)
    {
        $cities = [
            "Amman" => 1,
            "Zarqa" => 2,
            "Irbid" => 3,
            "AlSalt" => 4,
            "Karak" => 5,
            "Ma'an" => 6,
            "Mafraq" => 7,
            "Tafilah" => 8,
            "Madaba" => 9,
            "Jerash" => 10,
            "Ajloun" => 11,
            "Aqaba" => 12,
            "Russayfah" => 13,
            "Baqa'a & Ein Al-Basha" => 14,
            "Ramtha" => 15,
        ];

        return $cities[$city];
    }
}
