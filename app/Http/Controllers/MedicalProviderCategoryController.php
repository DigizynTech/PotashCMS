<?php

namespace App\Http\Controllers;

use App\Models\MedicalProvider;
use App\Models\MedicalProviderCategory;
use App\Models\Options;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MedicalProviderCategoryController extends Controller
{
    public function index(Request $request) {
        $parent = $request->get('parent');
        if ($request->has('parent')) {
            $category = MedicalProviderCategory::where('id',(int)$parent)->first();
            if ($category) {
                $categories = MedicalProviderCategory::where('parent', $parent)->get();
                return view('Departments.manage-sub', [
                    'category' => $category,
                    'categories' => $categories
                ]);
            }
        } else {
            $categories = MedicalProviderCategory::where('parent', 0)->get();
            return view('Departments.manage', [
                'categories' => $categories
            ]);
        }

    }
    public function create() {

    }
    public function store(Request $request) {
        if ($request->has('parent')) {
            MedicalProviderCategory::create([
                'name_ar' =>  $request->get('name_ar'),
                'name' =>  $request->get('name'),
                'parent' => (int)$request->get('parent')
            ]);
        } else {
            MedicalProviderCategory::create([
               'name_ar' =>  $request->get('name_ar'),
               'name' =>  $request->get('name'),
                'parent' => 0
            ]);
        }
        $version = Options::where('option_key','MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->back()->with('message', 'Category added successfully');
    }

    public function edit(Request $request, MedicalProviderCategory $cat) {

        return view('Departments.modify', [
            'category' => $cat,
            'route' => route('MdCat.update', $cat->id),
            'method' => 'PUT'
        ]);
    }
    public function update(Request $request, MedicalProviderCategory $cat) {
        $cat->update([
           'name' => $request->get('name'),
           'name_ar' => $request->get('name_ar'),
        ]);
        $version = Options::where('option_key','MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);
        if ($cat->parent == 0) {
            return redirect(route('MdCat.index'))->with('message','Category updated successfully.');
        } else {
            return redirect(route('MdCat.index',['parent' => $cat->parent]))->with('message','Category updated successfully.');
        }
    }
    public function delete(Request $request, MedicalProviderCategory $cat) {
        $medics = MedicalProvider::whereIn('category', [$cat->id,$cat->parent])->get()->count();
        if ($medics > 0) {
            return redirect()->back()->with('message','You can not remove category, there are medical providers associated with it.');
        }
        $cats = MedicalProviderCategory::where('parent',$cat->id)->get()->count();
        if ($cats > 0) {
            return redirect()->back()->with('message','You can not remove category, There are child categories associated.');
        }

        $cat->delete();
        $version = Options::where('option_key','MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);
        return redirect()->back()->with('message','Category deleted successfully');
    }
}
