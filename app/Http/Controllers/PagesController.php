<?php

namespace App\Http\Controllers;

use App\Models\Options;
use App\Models\User;
use Cache;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function dashboard()
    {

        $data = [
            'users' => User::count(),
            'android' => User::where('device_name','LIKE','android')->get()->count(),
            'ios' => User::where('device_name','LIKE','ios')->get()->count(),
        ];

        return view('Pages.dashboard', [
            'data' => $data
        ]);
    }

    public function reports()
    {
        return view('Pages.reports');

    }
}
