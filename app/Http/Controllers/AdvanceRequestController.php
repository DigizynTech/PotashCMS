<?php

namespace App\Http\Controllers;

use App\Models\AdvanceRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdvanceRequestController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('jobId') || $request->has('month')) {
            if ($request->get('month') == 'any') {
                $rows = AdvanceRequest::where('user_id', 'LIKE', $request->get('jobId'))->get();
            } else {
                $rows = AdvanceRequest::where('user_id', 'LIKE', $request->get('jobId'))
                    ->whereMonth('date', '=', Carbon::parse($request->get('month'))->format('m'))
                    ->whereYear('date', '=', Carbon::parse($request->get('month'))->format('Y'))
                    ->get();
            }
            $month = $request->get('month');
        } else {
            $month = Carbon::now()->format('M-Y');
            $rows = AdvanceRequest::whereMonth('date', '=', Carbon::now()->format('m'))
                ->whereYear('date', '=', Carbon::now()->format('Y'))
                ->get();
        }

        $start_month = Carbon::parse("30-11-2021");
        $diff = Carbon::now()->diffInMonths($start_month, true) + 1;
        $months = [$start_month->format('M-Y')];
        for ($i = 0; $i < $diff; $i++) {
            $months[] = $start_month->addMonthNoOverflow()->format('M-Y');
        }
        return view('RequestAdvanceSaving.index',
            [
                'rows' => $rows,
                'months' => $months,
                'month' => $month
            ]);
    }

    public function upload(Request $request)
    {
        $path = Storage::putFile('uploads', $request->file('upload'));

        $rows = array_map('str_getcsv', file(__DIR__ . '/../../../storage/app/' . $path));
        $index = 0;

        $headers = ['job_id', 'saving_account_balance', 'request_amount', 'payment_period', 'remaining_amount'];

        // File Validation
        //Number of columns
        if (count($rows[0]) != 5) {
            return redirect()->back()->with('message','CSV file is not valid, please use the appropriate template');
        }
        // Column names
        foreach ($rows[0] as $key => $value) {
            if ($value != $headers[$key]) {
                return redirect()->back()->with('message','CSV file is not valid, please use the appropriate template');
            };
        }

        foreach ($rows as $row) {
            if ($index == 0) {
                $index++;
                continue;
            }
            AdvanceRequest::create([
                'user_id' => $row[0] ?? 0,
                'saving_account_balance' => (float)$row[1] ?? 0,
                'request_amount' => (float)$row[2],
                'payment_period' => (float)$row[3],
                'remaining_amount' => (float)$row[4],
                'iban' => 0,
                'accepted' => 0,
                'date' => Carbon::parse($request->get('month')),
                'created_by' => \Auth::user()->id,
                'last_modified_by' => \Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            $index++;
        }
        return redirect('/advance-requests')->with('message', $index . ' user data has been added to the database');
    }

    public function edit(AdvanceRequest $req)
    {
        return view('RequestAdvanceSaving.modify', [
            'req' => $req,
            'route' => route('SvF.update', $req->id),
            'method' => 'PUT'
        ]);
    }

    public function update(Request $request, AdvanceRequest $req)
    {
        $req->update([
            'remaining_amount' => $request->get('remaining_amount'),
            'payment_period' => $request->get('payment_period'),
            'request_amount' => $request->get('request_amount'),
            'saving_account_balance' => $request->get('saving_account_balance'),
        ]);
        return redirect()->back()->with('message', 'Row updated successfully');
    }

    public function delete(AdvanceRequest $req)
    {
        $req->delete();
        return redirect()->back()->with('message', 'Row deleted successfully');
    }
}
