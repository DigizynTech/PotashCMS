<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\PotashDB;
use App\Models\AdvanceRequest;
use App\Models\News;
use App\Models\User;
use App\Models\UserData;
use Auth;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function index(): Factory|View|Application
    {
        $users = User::all();
        return view('Users.index', [
            'users' => $users
        ]);
    }

    public function show(User $user)
    {
        $potash = new PotashDB($user->job_id_number);
        $info = $potash->getEmployeeInfo();
        $medical_info = $potash->getMedicalInfo();
        $advance_request = AdvanceRequest::orderByDesc('date')->where('user_id', $user->job_id_number)->first();
        if ($advance_request) {
            $savings = $advance_request->saving_account_balance;
        } else {
            $savings = 0;
        }

        $attendance = $this->getUserAttendance($user->job_id_number);
        $leaves = $this->getUserLeaves($user->job_id_number);
        $vacations = $this->getUserVacations($user->job_id_number);
        try {
            $vacation_balance = $potash->getVacationBalance();
        } catch (\Exception $e) {
            $vacation_balance = 0;
        }
        return view('Users.modify', [
            'user' => $user,
            'info' => $info,
            'medical_info' => $medical_info,
            'vacation_balance' => $vacation_balance,
            'savings' => $savings,
            'attendance' => $attendance,
            'leaves' => $leaves,
            'vacations' => $vacations
        ]);
    }

    //Auth
    public function showLogin(): Factory|View|Application
    {
        return view('Auth.login');
    }

    public function doLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $user = User::where('email', (string)$request->get('email'))->first();
        if ($user) {
            if (!$user->accepted || Str::length($user->role) < 2) {
                return back()->withErrors([
                    'email' => 'You are not allowed!',
                ]);
            }
        }
        if (Auth::attempt($credentials, true)) {
            $request->session()->regenerate();
            return redirect()->intended('/dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function signout(Request $request): Redirector|Application|RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function getUserAttendance($userid)
    {
        $PotashDB = new PotashDB($userid);
        $attendance = $PotashDB->getEmployeeAttendance();
        $data = [];
        foreach ($attendance as $day) {
            try {
                $date = Carbon::parse($day['da_date'])->format('Y-m-d');
            } catch (InvalidFormatException $error) {
                $date = $day['da_date'];
            }
            $data[] = [
                'date' => $date,
                'from' => $this->formatTimeLessThan10($day['st_time_hh']) . ":" . $this->formatTimeLessThan10($day['st_time_mi']),
                'to' => $this->formatTimeLessThan10($day['en_time_hh']) . ":" . $this->formatTimeLessThan10($day['en_time_mi']),
                'type' => $day['transaction_type']
            ];
        }

        return $data;
    }

    public function getUserLeaves($userid)
    {
        $PotashDB = new PotashDB($userid);
        $day_leaves = $PotashDB->getEmployeeLeaves();
        $data = ['leaves' => []];
        foreach ($day_leaves as $day) {
            try {
                $date = Carbon::parse($day['date_from'])->format('Y-m-d');
            } catch (InvalidFormatException $error) {
                $date = $day['date_from'];
            }


            $data['leaves'][] = [
                'date' => $date,
                'from' => '',
                'to' => '',
                'type' => $day['sub_tran_type'],
                'Interval' => $day['total_hour']
            ];
        }

        $leaves = collect($data['leaves']);
        $ordered = $leaves->sortByDesc(function ($vacation) {
            return Carbon::parse($vacation['date'] . ' ' . $vacation['from'])->timestamp;
        });
        $data['leaves'] = $ordered->values();
    }

    public function getUserVacations($userid)
    {
        $PotashDB = new PotashDB($userid);
        $vacations = $PotashDB->getVacationHistory();

        $data = [];
        foreach ($vacations as $vacation) {
            $interval = Carbon::parse($vacation['abs_date_from'])->diffInDays(Carbon::parse($vacation['abs_date_to'])) + 1;
            $data[] = [
                'from' => $vacation['abs_date_from'],
                'to' => $vacation['abs_date_to'],
                'type' => $vacation['absence_type'],
                'Interval' => $interval,
                'reason' => $vacation['sick_reason']
            ];
        }

        $vacations = collect($data);
        $ordered = $vacations->sortByDesc(function ($vacation) {
            return Carbon::parse($vacation['from'])->timestamp;
        });
        return $ordered->values();
    }

    private function formatTimeLessThan10($time): string
    {
        $time = (int)$time;
        if ($time < 10) {
            return "0" . $time;
        } else {
            return $time;
        }
    }

    public function moderate(Request $request)
    {
        $user = User::where('id', $request->get('user_id'))->first();
        if (Str::length($request->get('password')) >= 4) {
            $user->update([
                'password' => \Hash::make($request->get('password')),
            ]);
        };
        $user->update([
            'role' => $request->collect('privileges')->implode(','),
        ]);
        $extra = "Password must be longer than 3 characters";

        return redirect()->back()->with('message', 'User info updated');
    }

    public function create()
    {
        return view('Users.create');
    }

    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'jobid' => 'required',
            'name' => 'required',
            'password' => 'required'
        ]);
        $potash = new PotashDB($request->get('jobid'));
        $e_info = $potash->getEmployeeInfo();
        $user = User::create([
            'job_id_number' => $request->get('jobid'),
            'name' => $request->get('name'),
            'email' => $request->get('jobid') . '@arabpotash.com',
            'password' => Hash::make($request->get('password')),
            'department' => $e_info['organization_name'] ?? '',
            'job_title' => $e_info['job_name'] ?? '',
            'fullname' => $e_info['emp_name_arb'] ?? ''
        ]);

        return redirect('/users/' . $user->id);
    }

    public function delete(User $user)
    {
        $user->update([
            'email' => 'deleted_account_jobid_' . $user->job_id_number,
            'password' => 'deleted password',
            'job_id_number' => Str::random('32'),
            'deleted_by' => Auth::user()->id,
        ]);

        //Fixes error that happen when a previous notification is sent to the deleted user
        News::where('employeeId',$user->id)->delete();

        $user->delete();

        return redirect()->back()->with('message','User deleted successfully');
    }
}
