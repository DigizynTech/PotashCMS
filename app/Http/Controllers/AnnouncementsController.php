<?php /** @noinspection DuplicatedCode */

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Laravel\Firebase\Facades\Firebase;

class AnnouncementsController extends Controller
{
    public function index()
    {
        $announcements = Announcement::orderByDesc('created_at')->get();
        return view('Announcements.index', [
            'announcements' => $announcements
        ]);
    }

    public function create()
    {
        $announcement = new Announcement();
        $announcement->published_at = Carbon::now()->format('Y-m-d');
        return view('Announcements.modify', [
            'announcement' => $announcement,
            'method' => '',
            'route' => '/announcements'
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
//            'language' => 'required',
//            'status' => 'required',
            'title' => 'required',
//            'date' => 'required',
//            'brief' => 'required',
            'content' => 'required',
//            'departmentId' => 'required',
//            'employeeId' => 'required'
        ]);

        $announcement = Announcement::create([
//            'language' => $request->get('language'),
//            'status' => $request->get('status'),
            'language' => 'arabic',
            'status' => 'published',
            'title' => $request->get('title'),
//            'brief' => $request->get('brief'),
            'brief' => '',
            'content' => $request->get('content'),
//            'published_at' => $request->get('date'),
            'published_at' => Carbon::now(),
//            'departmentId' => (int)$request->get('departmentId'),
//            'employeesId' => (int)$request->get('employeeId'),
            'departmentId' => 0,
            'employeesId' => 0,
            'created_by' => Auth::user()->id,
            'last_updated_by' => Auth::user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if ($request->get('hasTextLink') == "true") {
            $announcement->update([
                'attachment' => $request->get('textLink')
            ]);
        } else {
            if ($request->hasFile('attachment')) {
                try {
                    $filename = $announcement->id.'_'.Str::random(6).'.'.$request->file('attachment')->extension();
                    $announcement->addMediaFromRequest('attachment')->setFileName($filename)->toMediaCollection('attachments');
                    $announcement->update([
                        'attachment' => $announcement->getFirstMediaUrl('attachments')
                    ]);
                } catch (FileDoesNotExist $e) {
                    return redirect()->back()->withErrors("File does not exist. --> " . $e->getMessage());
                } catch (FileIsTooBig $e) {
                    return redirect()->back()->withErrors("File is too large to handle. --> " . $e->getMessage());
                }
            }
        }

        // Send notification to all users
        $deviceTokens = User::whereNotIn('device_token', [''])
            ->whereNotNull('device_token')
            ->get()->pluck('device_token')->toArray();

        if (count($deviceTokens) != 0) {
            $message = CloudMessage::new()->withNotification(Notification::create("تم إضافة تعميم جديد!",""));
            $tokens_array = array_chunk($deviceTokens, 499);
            foreach ($tokens_array as $tokens) {
                Firebase::messaging()->sendMulticast($message, $tokens);
                sleep(2);
            }
            return redirect()->back()->with('message', 'Notification is sent to ' . count($deviceTokens) . ' Employees');
        }

        return redirect(route('Announcements.edit', $announcement->id))->with('message','Announcement sent');
    }

    public function show(Announcement $news)
    {
    }

    public function edit(Announcement $announcement)
    {
        return view('Announcements.modify', [
            'announcement' => $announcement,
            'method' => 'put',
            'route' => "/announcements/" . $announcement->id
        ]);
    }


    public function update(Request $request, Announcement $announcement): Redirector|Application|RedirectResponse
    {
        $validated = $request->validate([
//            'language' => 'required',
//            'status' => 'required',
            'title' => 'required',
//            'date' => 'required',
//            'brief' => 'required',
            'content' => 'required',
//            'departmentId' => 'required',
//            'employeeId' => 'required'
        ]);

        $announcement->update([
//            'language' => $request->get('language'),
//            'status' => $request->get('status'),
            'language' => 'arabic',
            'status' => 'published',
            'title' => $request->get('title'),
//            'brief' => $request->get('brief'),
            'content' => $request->get('content'),
//            'published_at' => $request->get('date'),
            'published_at' => Carbon::now(),
//            'departmentId' => (int)$request->get('departmentId'),
//            'employeesId' => (int)$request->get('employeeId'),
            'departmentId' => 0,
            'employeesId' => 0,
            'last_updated_by' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);

        if ($request->get('hasTextLink') == "true") {
            $announcement->update([
                'attachment' => $request->get('textLink')
            ]);
        } else {
            if ($request->hasFile('attachment')) {
                try {
                    $filename = $announcement->id.'_'.Str::random(6).'.'.$request->file('attachment')->extension();
                    $announcement->addMediaFromRequest('attachment')->setFileName($filename)->toMediaCollection('attachments');
                    $announcement->update([
                        'attachment' => $announcement->getFirstMediaUrl('attachments')
                    ]);
                } catch (FileDoesNotExist $e) {
                    return redirect()->back()->withErrors("File does not exist. --> " . $e->getMessage());
                } catch (FileIsTooBig $e) {
                    return redirect()->back()->withErrors("File is too large to handle. --> " . $e->getMessage());
                }
            }
        }

        return redirect(route('Announcements.edit', $announcement->id))->with('message','Announcement sent');
    }

    public function delete(Announcement $announcement)
    {
        $announcement->delete();
        return redirect(route('Announcements.index'))->with('message','Announcement #' . $announcement->id . ' has been deleted');
    }
}
