<?php

namespace App\Http\Controllers;

use App\Models\Ideas;
use App\Models\MedicalProvider;
use App\Models\MedicalProviderCategory;
use App\Models\Options;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Laravel\Firebase\Facades\Firebase;

class MedicalProviderController extends Controller
{

    public function index(Request $request)
    {
        $cities = [
            "All" => 0,
            "Amman" => 1,
            "Zarqa" => 2,
            "Irbid" => 3,
            "AlSalt" => 4,
            "Karak" => 5,
            "Ma'an" => 6,
            "Mafraq" => 7,
            "Tafilah" => 8,
            "Madaba" => 9,
            "Jerash" => 10,
            "Ajloun" => 11,
            "Aqaba" => 12,
            "Russayfah" => 13,
            "Baqa'a & Ein Al-Basha" => 14,
            "Ramtha" => 15,
        ];
        $cats = MedicalProviderCategory::where('parent', 0)->get();
        $categories = [['id' => 0, 'name' => 'All']];
        foreach ($cats as $cat) {
            $categories[] = [
                'id' => (int)$cat->id,
                'name' => $cat->name
            ];
        }
        $subCategories = [];
        foreach ($cats as $category) {
            $subs = MedicalProviderCategory::where('parent', $category->id)->get();
            foreach ($subs as $sub) {
                $subCategories[] = [
                    'id' => (int)$sub->id,
                    'name' => $sub->name,
                    'parent' => (int)$sub->parent
                ];
            }
        }

        $parent_cat = $request->get('category') ?? 0;
        $child_cat = $request->get('sub_category') ?? 0;

        if ($request->has('search')) {
            $keyword = $request->get('search');
            $words = [];
            if (Str::contains($keyword, 'ا')) {
                $words = ["%" . $keyword . "%", "%" . Str::replace('ا', 'أ', $keyword) . "%"];
            } else if (Str::contains($keyword, 'أ')) {
                $words = ["%" . $keyword . "%", "%" . Str::replace('أ', 'ا', $keyword) . "%"];
            } else {
                $words = ["%" . $keyword . "%", "%" . $keyword . "%"];
            }
            $providers = MedicalProvider::where(function($query) use ($words) {
                $query->where('name_ar', 'LIKE', $words[0])->orWhere('name_ar', 'LIKE', $words[1]);
            });
            if ($parent_cat != 0) {
                if ($child_cat == 0) {
                    $categories = MedicalProviderCategory::where('parent', $parent_cat)->get();
                    if ($categories->count() != 0) {
                        $selected_categories = [$parent_cat];
                    } else {
                        $selected_categories = $categories->pluck('id');
                    }
                    $providers = $providers->whereIn('category', $selected_categories);
                } else {
                    $providers = $providers->where('category', $child_cat);
                }
            }
            if ($request->get('city') != 0) {
                $providers = $providers->where('city', $request->get('city'));
            }

            $providers = $providers->with('MedicalCategory')->get();
        } else {
            $providers = MedicalProvider::with('MedicalCategory')->get();
        }
        return view('MedicalProviders.index', [
            'providers' => $providers,
            'categories' => $categories,
            'subCategories' => $subCategories,
            'selected_cat' => $parent_cat,
            'selected_sub_cat' => $child_cat,
            'cities' => $cities,
        ]);
    }

    public function create()
    {
        $cities = [
            "Amman" => 1,
            "Zarqa" => 2,
            "Irbid" => 3,
            "AlSalt" => 4,
            "Karak" => 5,
            "Ma'an" => 6,
            "Mafraq" => 7,
            "Tafilah" => 8,
            "Madaba" => 9,
            "Jerash" => 10,
            "Ajloun" => 11,
            "Aqaba" => 12,
            "Russayfah" => 13,
            "Baqa'a & Ein Al-Basha" => 14,
            "Ramtha" => 15,
        ];
        $provider = new MedicalProvider();
        $cats = MedicalProviderCategory::where('parent', 0)->get();
        $categories = [];
        foreach ($cats as $cat) {
            $categories[] = [
                'id' => (int)$cat->id,
                'name' => $cat->name
            ];
        }
        $subCategories = [];
        foreach ($cats as $category) {
            $subs = MedicalProviderCategory::where('parent', $category->id)->get();
            foreach ($subs as $sub) {
                $subCategories[] = [
                    'id' => (int)$sub->id,
                    'name' => $sub->name,
                    'parent' => (int)$sub->parent
                ];
            }
        }

        $parent_cat = 1;
        $child_cat = 1;

        return view('MedicalProviders.modify', [
            'provider' => $provider,
            'categories' => $categories,
            'subCategories' => $subCategories,
            'selected_cat' => $parent_cat,
            'selected_sub_cat' => $child_cat,
            'route' => route('Md.store'),
            'method' => '',
            'cities' => $cities
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'name_ar' => 'required',
            'address' => 'required',
            'address_ar' => 'required',
            'phone' => 'nullable',
            'phone_2' => 'nullable',
            'category' => 'required',
            'sub_category' => 'required',
            'city' => 'required'
        ]);

        $md = MedicalProvider::create([
            'name' => $validated['name'],
            'name_ar' => $validated['name_ar'],
            'address' => $validated['address'],
            'address_ar' => $validated['address_ar'],
            'phone' => $validated['phone'],
            'phone_2' => $validated['phone_2'],
            'category' => $validated['sub_category'],
            'city' => $validated['city']
        ]);

        $version = Options::where('option_key', 'MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);


        //Send notification to all employees
        $message = CloudMessage::new()->withNotification(Notification::create("شركة البوتاس العربية", "تم تحديث قاعدة البيانات الطبية"));
        $deviceTokens = User::whereNotIn('device_token', [''])
            ->whereNotNull('device_token')
            ->get()->pluck('device_token')->toArray();

        $tokens_array = array_chunk($deviceTokens, 499);
        foreach ($tokens_array as $tokens) {
            Firebase::messaging()->sendMulticast($message, $tokens);
            sleep(2);
        }

        return redirect(route('Md.edit', $md->id))->with('message', 'Medical provider updated successfully and notifications sent to all employees');
    }


    public function edit(MedicalProvider $provider)
    {
        $cities = [
            "Amman" => 1,
            "Zarqa" => 2,
            "Irbid" => 3,
            "AlSalt" => 4,
            "Karak" => 5,
            "Ma'an" => 6,
            "Mafraq" => 7,
            "Tafilah" => 8,
            "Madaba" => 9,
            "Jerash" => 10,
            "Ajloun" => 11,
            "Aqaba" => 12,
            "Russayfah" => 13,
            "Baqa'a & Ein Al-Basha" => 14,
            "Ramtha" => 15,
        ];
        $cats = MedicalProviderCategory::where('parent', 0)->get();
        $categories = [];
        foreach ($cats as $cat) {
            $categories[] = [
                'id' => (int)$cat->id,
                'name' => $cat->name
            ];
        }
        $subCategories = [];
        foreach ($cats as $category) {
            $subs = MedicalProviderCategory::where('parent', $category->id)->get();
            foreach ($subs as $sub) {
                $subCategories[] = [
                    'id' => (int)$sub->id,
                    'name' => $sub->name,
                    'parent' => (int)$sub->parent
                ];
            }
        }

        $parent_cat = $provider->MajorMedicalField();
        $child_cat = $provider->category;

        return view('MedicalProviders.modify', [
            'provider' => $provider,
            'categories' => $categories,
            'subCategories' => $subCategories,
            'selected_cat' => $parent_cat,
            'selected_sub_cat' => $child_cat,
            'route' => route('Md.update', $provider->id),
            'method' => 'PUT',
            'cities' => $cities
        ]);
    }

    public function update(Request $request, MedicalProvider $provider)
    {
        $validated = $request->validate([
            'name' => 'required',
            'name_ar' => 'required',
            'address' => 'required',
            'address_ar' => 'required',
            'phone' => 'nullable',
            'phone_2' => 'nullable',
            'category' => 'required',
            'sub_category' => 'required',
            'city' => 'required'
        ]);

        $provider->update([
            'name' => $validated['name'],
            'name_ar' => $validated['name_ar'],
            'address' => $validated['address'],
            'address_ar' => $validated['address_ar'],
            'phone' => $validated['phone'],
            'phone_2' => $validated['phone_2'],
            'category' => $validated['sub_category'],
            'city' => $validated['city']
        ]);

        $version = Options::where('option_key', 'MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);

        return redirect(route('Md.edit', $provider->id))->with('message', 'Medical provider updated successfully');
    }

    public function delete(MedicalProvider $provider)
    {
        $provider->delete();
        $version = Options::where('option_key', 'MedicalProvidersVersion')->first();
        $version->update([
            'option_value' => (int)$version->option_value + 1,
            'updated_at' => Carbon::now(),
        ]);
        return redirect(route('Md.index'))->with('message', 'Medical Provider deleted successfully');
    }

    public function export() {
        $fileName = 'medical-providers-'. Carbon::now()->format('d-m-Y-H-s-i') .'.csv';
        $medicalProviders = MedicalProvider::all();

        $headers = array(
            "Content-type"        => "text/csv; charset=utf-8",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('id', 'name', 'name_ar', 'address', 'address_ar','phone','phone_2', 'category','sub_category', 'city');

        $callback = function() use($medicalProviders, $columns) {
            $file = fopen('php://output', 'w');
            fputs($file,"\xEF\xBB\xBF");
            fputcsv($file, $columns);

            foreach ($medicalProviders as $medicalProvider) {
                $row['id']  = $medicalProvider->id;
                $row['name']  = $medicalProvider->name;
                $row['name_ar']  = $medicalProvider->name_ar;
                $row['address']  = $medicalProvider->address;
                $row['address_ar']  = $medicalProvider->address_ar;
                $row['phone']  = $medicalProvider->phone;
                $row['phone_2']  = $medicalProvider->phone_2;
                $row['category']  = MedicalProviderCategory::where('id',$medicalProvider->MajorMedicalField())->first()->name;
                $row['sub_category']  = $medicalProvider->MedicalCategory->name;
                $row['city']  = $this->cityName((int)$medicalProvider->city);
//                $row['category']  = '1';
//                $row['sub_category']  = '2';
//                $row['city']  = '3';


                fputcsv($file, array($row['id'], $row['name'], $row['name_ar'],
                    $row['address'], $row['address_ar'], $row['phone'], $row['phone_2']
                , $row['category'], $row['sub_category'], $row['city']));
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function manageCategories()
    {
        $categories = MedicalProviderCategory::where('parent', 0)->get();
        return view('Departments.manage', [
            'categories' => $categories
        ]);
    }

    public function manageSubCategories(MedicalProviderCategory $category)
    {
        $categories = MedicalProviderCategory::where('parent', 1)->get();
        return view('Departments.manage-sub', [
            'categories' => $categories,
            'category' => $category
        ]);
    }

    public function cityName(int $id) {
        $cities = [
            "Amman" => 1,
            "Zarqa" => 2,
            "Irbid" => 3,
            "AlSalt" => 4,
            "Karak" => 5,
            "Ma'an" => 6,
            "Mafraq" => 7,
            "Tafilah" => 8,
            "Madaba" => 9,
            "Jerash" => 10,
            "Ajloun" => 11,
            "Aqaba" => 12,
            "Russayfah" => 13,
            "Baqa'a & Ein Al-Basha" => 14,
            "Ramtha" => 15,
        ];

        return array_search($id, $cities);
    }
}
