<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\PotashDB;
use App\Models\News;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Laravel\Firebase\Facades\Firebase;

class NewsController extends Controller
{
    public function index()
    {
        $articles = News::where('id', '>', 0)->orderByDesc('created_at')->get();
        return view('News.index', [
            'articles' => $articles
        ]);
    }

    public function create()
    {
        $article = new News();
        $article->employeeId = 0;
        $potash = new PotashDB(0);
        $departments = $potash->getAllDepartments();
        $users = User::all();
        $employees = [[
            'id' => 0,
            'name' => 'All',
            'department' => 'All',
        ]];
        foreach ($users as $user) {
            $employees[] = [
                'id' => $user->id,
                'name' => $user->fullname,
                'department' => $user->department
            ];
        }

        $departments = array_merge(['All'], $departments);
        return view('News.modify', [
            'article' => $article,
            'departments' => $departments,
            'employees' => $employees,
            'route' => route('News.store'),
            'method' => '',
        ]);
    }

    public function store(Request $request): Redirector|Application|RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'fullContent' => 'required',
            'departmentId' => 'required',
            'employeeId' => 'required'
        ]);

        $news = News::create([
            'title' => $validated['title'],
            'content' => $validated['content'],
            'full' => $validated['fullContent'],
            'created_by' => Auth::user()->id,
            'last_updated_by' => Auth::user()->id,
            'departmentId' => $validated['departmentId'],
            'employeeId' => $validated['employeeId'],
            'sent' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return redirect(route('News.index'))->with('message', 'Notification #' . $news->id . ' created/updated. Ready to push to user/s');
    }

    public function edit(News $news)
    {
        $potash = new PotashDB(0);
        $departments = $potash->getAllDepartments();
        $users = User::all();
        $employees = [[
            'id' => 0,
            'name' => 'All',
            'department' => 'All',
        ]];
        foreach ($users as $user) {
            $employees[] = [
                'id' => $user->id,
                'name' => $user->fullname,
                'department' => $user->department
            ];
        }
        $departments = array_merge(['All'], $departments);
        return view('News.modify', [
            'article' => $news,
            'departments' => $departments,
            'employees' => $employees,
            'route' => route('News.update', $news->id),
            'method' => 'put',
        ]);

    }

    public function update(Request $request, News $news): Redirector|Application|RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'fullContent' => 'required',
            'departmentId' => 'required',
            'employeeId' => 'required'
        ]);

        $news->update([
            'title' => $validated['title'],
            'content' => $validated['content'],
            'full' => $validated['fullContent'],
            'last_updated_by' => Auth::user()->id,
            'departmentId' => $validated['departmentId'],
            'employeeId' => $validated['employeeId'],
            'sent' => 0,
            'updated_at' => Carbon::now()
        ]);

        return redirect(route('News.index'))->with('message', 'Notification #' . $news->id . ' created/updated. Ready to push to user/s');
    }

    public function delete(News $news): RedirectResponse
    {
        $news->delete();
        return redirect(route('News.index'))->with('message', 'Notification #' . $news->id . ' has been deleted');
    }

    /**
     * @throws \Kreait\Firebase\Exception\MessagingException
     * @throws \Kreait\Firebase\Exception\FirebaseException
     */
    public function push(News $news)
    {
        $department = $news->departmentId;
        $employee = $news->employeeId;

        if ($department == 'All' && $employee == 0) {
            $deviceTokens = User::whereNotIn('device_token', [''])
                ->whereNotNull('device_token')
                ->get()->pluck('device_token')->toArray();
        } else {
            if ($employee != 0) {
                $deviceTokens = User::where('id', $employee)
                    ->whereNotIn('device_token', [''])
                    ->whereNotNull('device_token')
                    ->get()->pluck('device_token')->toArray();
            } else {
                $deviceTokens = User::where('department', $department)
                    ->whereNotIn('device_token', [''])
                    ->whereNotNull('device_token')
                    ->get()->pluck('device_token')->toArray();
            }
        }
        if (count($deviceTokens) != 0) {
            $message = CloudMessage::new()->withNotification(Notification::create($news->title, strip_tags($news->content)));
            $tokens_array = array_chunk($deviceTokens, 499);
            foreach ($tokens_array as $tokens) {
                Firebase::messaging()->sendMulticast($message, $tokens);
                sleep(2);
            }
            $news->update([
                'sent' => true,
            ]);
            return redirect()->back()->with('message', 'Notification is sent to ' . count($deviceTokens) . ' Employees');

        } else {
            return redirect()->back()->with('message', 'No Notification were sent. ' . count($deviceTokens) . ' Employees for the requested filter');
        }
    }
}
