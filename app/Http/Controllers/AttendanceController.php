<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function index(Request $request)
    {
        $date = $request->get('date') ?? Carbon::now()->format('Y-m-d');
        $attendance = Attendance::where('date', '=', $date)->get();

        return view('Attendance.index', [
            'attendance' => $attendance,
            'date' => $date
        ]);

    }
}
