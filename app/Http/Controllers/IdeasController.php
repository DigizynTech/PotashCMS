<?php

namespace App\Http\Controllers;

use App\Models\Ideas;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class IdeasController extends Controller
{
    public function index()
    {
        $ideas = Ideas::all()->sortByDesc('created_at');

        return view('Ideas.index', [
            'ideas' => $ideas
        ]);
    }


    public function show(Ideas $idea)
    {
        $categories = [
            1 => 'العمليات ورفع الكفاءة والقدرة الإنتاجية',
            2 => 'بيئة العمل',
            3 => 'رفع جودة المنتج',
            4 => 'كفاءة وترشيد استهلاك الطاقة والمياه',
            5 => 'خفض الكلف والنفقات',
            6 => 'أخرى',
        ];


        $idea_cats = explode(',', $idea->categories);

        $cs = [];
        foreach ($idea_cats as $cat) {
            try {
                $cs[] = $categories[$cat];
            } catch (\Exception $e) {
                $cs[] = '--';
            }
        }


        return view('Ideas.modify', [
            'idea' => $idea,
            'categories' => implode('&#13;&#10;', $cs),
        ]);
    }

    public function export(Request $request)
    {
        $categories = [
            1 => 'العمليات ورفع الكفاءة والقدرة الإنتاجية',
            2 => 'بيئة العمل',
            3 => 'رفع جودة المنتج',
            4 => 'كفاءة وترشيد استهلاك الطاقة والمياه',
            5 => 'خفض الكلف والنفقات',
            6 => 'أخرى',
        ];


        $fileName = 'creative-ideas-' . Carbon::now()->format('d-m-Y-H-s-i') . '.csv';
        if ($request->get('start_date') == null || $request->get('end_date') == null) {
            $ideas = Ideas::all()->sortByDesc('created_by');
        } else {
            $start = Carbon::createFromFormat('Y-m-d', $request->get('start_date'));
            $end = Carbon::createFromFormat('Y-m-d', $request->get('end_date'));
            $ideas = Ideas::all()->whereBetween('created_at', [$start, $end])->sortByDesc('created_by');
        }

        $headers = array(
            "Content-type" => "text/csv; charset=utf-8",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('id', 'date', 'title', 'categories', 'description', 'tools', 'steps', 'sent_by', 'position', 'department');

        $callback = function () use ($ideas, $columns, $categories) {
            $file = fopen('php://output', 'w');
            fputs($file, "\xEF\xBB\xBF");
            fputcsv($file, $columns);

            foreach ($ideas as $idea) {
                $idea_cats = explode(',', $idea->categories);

                $cs = [];
                foreach ($idea_cats as $cat) {
                    try {
                        $cs[] = $categories[$cat];
                    } catch (\Exception $e) {
                        $cs[] = '--';
                    }
                }
                $cs = implode(',', $cs);

                $row['id'] = $idea->id;
                $row['date'] = $idea->created_at->format('d/m/y');
                $row['title'] = $idea->title;
                $row['categories'] = $cs;
                $row['description'] = $idea->description;
                $row['tools'] = $idea->tools;
                $row['steps'] = $idea->steps;
                $row['sent_by'] = $idea->user->fullname;
                $row['job_title'] = $idea->user->job_title;
                $row['department'] = $idea->user->department;


                fputcsv($file, array($row['id'], $row['date'], $row['title'],
                    $row['categories'], $row['description'], $row['tools'], $row['steps']
                , $row['sent_by'], $row['job_title'], $row['department']));
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
