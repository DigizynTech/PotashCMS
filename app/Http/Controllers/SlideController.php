<?php

namespace App\Http\Controllers;

use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\ErrorHandler\Exception\SilencedErrorContext;

class SlideController extends Controller
{
    public function index()
    {
        $slides = Slide::all();

        return view('Slides.index', [
            'slides' => $slides
        ]);
    }


    public function create()
    {
        $slide = new Slide();
        return view('Slides.modify', [
            'slide' => $slide,
            'route' => route('Slides.store'),
            'method' => ''
        ]);
    }

    public function edit(Slide $slide)
    {
        return view('Slides.modify', [
            'slide' => $slide,
            'route' => route('Slides.update', $slide->id),
            'method' => 'PUT'
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'image' => 'required|dimensions:width=480,height=192'
        ]);
        $slide = Slide::create([
            'link'=> ''
        ]);
        $slide->addMediaFromRequest('image')->toMediaCollection('images');
        $slide->update([
           'link' => $slide->getFirstMediaUrl('images'),
        ]);

        return redirect(route('Slides.index'))->with('message','Slide added successfully');
    }

    public function update(Request $request, Slide $slide)
    {
        $validated = $request->validate([
            'image' => 'required|dimensions:width=480,height=192'
        ]);
        $slide->addMediaFromRequest('image')->toMediaCollection('images');
        $slide->update([
            'link' => $slide->getFirstMediaUrl('images'),
        ]);

        return redirect(route('Slides.index'))->with('message','Slide updated successfully');
    }

    public function delete(Slide $slide)
    {
        $slide->delete();

        return redirect(route('Slides.index'))->with('message','Slide deleted successfully');

    }

    public function api() {

        $slides = Slide::all();

        $array = [
            'status' => 200,
            'response' => []
        ];

        foreach ($slides as $slide) {
            $array['response'][] = $slide->link;
        }


        return Response::json($array);
    }
}
