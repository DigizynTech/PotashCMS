<?php

namespace App\Http\Livewire\Users;

use App\Models\User;
use Livewire\Component;

class Toggle extends Component
{
    public int $user_id;
    public bool $status;

    public function like()
    {
        $user = User::where('id', $this->user_id)->first();
        $user->update([
            'accepted' => !$user->accepted,
            'accepted_by' => \Auth::user()->id,
        ]);
        $this->status = !$this->status;
    }

    public function mount($user_id, $status)
    {
        $this->user_id = $user_id;
        $this->status = $status;
    }

    public function render()
    {
        return view('livewire.users.toggle');
    }
}
