<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $role
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role): mixed
    {
        if ($request->user()->role == "admin") {
            return $next($request);
        } else if (in_array($role, explode(",", $request->user()->role))) {
            return $next($request);
        }
        return abort(403);
    }
}
