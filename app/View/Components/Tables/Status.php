<?php

namespace App\View\Components\Tables;

use Illuminate\View\Component;

class Status extends Component
{
    public string $status;
    public string $classes;
    public string $text;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $status)
    {
        $this->status = $status;
        $this->classes = "bg-red-100 text-red-800";
        $this->text = "Private";
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if ($this->status == true) {
            $this->classes = "bg-green-100 text-green-800";
            $this->text = "Published";
        }

        return view('components.tables.status');
    }
}
