<?php

namespace App\View\Components\Tables;

use Illuminate\View\Component;

class toggle extends Component
{
    public bool $status = false;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(bool $status)
    {
        $this->status = $status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.tables.toggle');
    }
}
