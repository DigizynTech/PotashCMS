<?php

namespace App\View\Components\sidebar;

use Illuminate\View\Component;

class item extends Component
{

    public string $name;
    public string $link;
    public string $icon;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name = "default", string $link = "#", string $icon = "face")
    {
        $this->name = $name;
        $this->link = $link;
        $this->icon = $icon;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sidebar.item');
    }
}
