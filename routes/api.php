<?php

use App\Http\Controllers\Api\AnnouncementsController;
use App\Http\Controllers\Api\MedicalProviderController;
use App\Http\Controllers\Api\NotificationsController;
use App\Http\Controllers\Api\UserData;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\IdeasController;
use App\Http\Controllers\SlideController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/profile', [UsersController::class, 'profile']);
    Route::get('/profile/dashboard', [UsersController::class, 'dashboard']);
    Route::get('/attendance', [UsersController::class, 'attendance']);
    Route::get('/house-loan',[UsersController::class,'getHouseLoan']);
    Route::get('/day-leave', [UsersController::class, 'getDayLeaves']);
    Route::get('/work-leave', [UsersController::class, 'getWorkLeaves']);
    Route::get('/advance-request', [UsersController::class, 'getAdvanceRequest']);
    Route::post('/advance-request', [UsersController::class, 'advance_request']);
    Route::post('/day-leave', [UsersController::class, 'dayLeave']);
    Route::post('/work-leave', [UsersController::class, 'workLeave']);
    Route::post('/ideas', [IdeasController::class,'store']);
    Route::get('/medical-info',[UsersController::class,'MedicalInfo']);
//    Route::get('/medical-providers',[,'']);
    Route::post('/test', function () {
        $user = Auth::user()->job_id_number;
        $url = config('database.connections.mysql.host') . '/api/v1/EmployeeInfo';
        $call = Http::withToken(config('auth.DB_APP_TOKEN'))->get($url, ['job_id' => $user]);
        Http::withToken(config('auth.DB_APP_TOKEN'))->asJson()->get($url, ['employee_id' => 6694])->body();
    });
    Route::get('/notifications', [NotificationsController::class, 'all']);
});

//Announcements
Route::get('/announcements', [AnnouncementsController::class, 'all']);
Route::get('/announcements/{id}', [AnnouncementsController::class, 'show']);
Route::get('/user/{id}/data', [UserData::class, 'userData']);
Route::get('/medical-providers', [MedicalProviderController::class, 'getAllMedicalProviders']);
Route::get('/medical-providers-version', [MedicalProviderController::class, 'MedicalProviderVersion']);
Route::get('/medical-providers-categories', [MedicalProviderController::class, 'MedicalProvidersCategories']);
Route::get('/medical-providers-sub-categories', [MedicalProviderController::class, 'MedicalProvidersSubCategories']);

Route::get('/slides', [SlideController::class,'api']);
//Auth
Route::post('/auth', [UsersController::class, 'auth']);
Route::post('register', [UsersController::class, 'register']);
