<?php

use App\Http\Controllers\AdvanceRequestController;
use App\Http\Controllers\AnnouncementsController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\IdeasController;
use App\Http\Controllers\MedicalProviderCategoryController;
use App\Http\Controllers\MedicalProviderController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\SlideController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProfilesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

//Dashboard and Reports
Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [PagesController::class, 'dashboard']);
    Route::get('/reports', [PagesController::class, 'reports']);
});
// Users Management
Route::prefix('users')->middleware(['auth', 'role:users'])->group(function () {
    Route::get('/', [UsersController::class, 'index']);
    Route::post('/moderation', [UsersController::class, 'moderate']);
    Route::get('/create', [UsersController::class, 'create']);
    Route::post('/store', [UsersController::class, 'store']);
    Route::get('/{user}', [UsersController::class, 'show']);
    Route::delete('/{user}', [UsersController::class, 'delete'])->name('users.delete');
});

//News
Route::prefix('news')->middleware(['auth', 'role:notifications'])->group(function () {
    Route::get('/', [NewsController::class, 'index'])->name('News.index');
    Route::get('/create', [NewsController::class, 'create'])->name('News.create');
    Route::post('/', [NewsController::class, 'store'])->name('News.store');
    Route::get('/{news}', [NewsController::class, 'show']);
    Route::get('/{news}/edit', [NewsController::class, 'edit'])->name('News.modify');
    Route::post('/{news}/push', [NewsController::class, 'push'])->name('News.push');
    Route::put('/{news}', [NewsController::class, 'update'])->name('News.update');;
    Route::delete('/{news}', [NewsController::class, 'delete'])->name('News.delete');

});

//Announcements
Route::prefix('announcements')->middleware(['auth', 'role:announcements'])->group(function () {
    Route::get('/', [AnnouncementsController::class, 'index'])->name('Announcements.index');
    Route::get('/create', [AnnouncementsController::class, 'create'])->name('Announcements.create');
    Route::post('/', [AnnouncementsController::class, 'store']);
    Route::get('/{announcement}', [AnnouncementsController::class, 'show']);
    Route::get('/{announcement}/edit', [AnnouncementsController::class, 'edit'])->name('Announcements.edit');
    Route::put('/{announcement}', [AnnouncementsController::class, 'update'])->name('Announcements.update');
    Route::delete('/{announcement}', [AnnouncementsController::class, 'delete'])->name('Announcements.delete');
});

Route::prefix('slides')->middleware(['auth','role:announcements'])->group( function() {
    Route::get('/', [SlideController::class, 'index'])->name('Slides.index');
    Route::get('/create', [SlideController::class, 'create'])->name('Slides.create');
    Route::post('/', [SlideController::class, 'store'])->name('Slides.store');
    Route::get('/{slide}', [SlideController::class, 'show']);
    Route::get('/{slide}/edit', [SlideController::class, 'edit'])->name('Slides.edit');
    Route::put('/{slide}', [SlideController::class, 'update'])->name('Slides.update');
    Route::delete('/{slide}', [SlideController::class, 'delete'])->name('Slides.delete');
});

//Medical Provider
Route::prefix('medical-providers')->middleware(['auth', 'role:medical'])->group(function () {
    Route::get('/', [MedicalProviderController::class, 'index'])->name('Md.index');
    Route::get('/create', [MedicalProviderController::class, 'create'])->name('Md.create');
    Route::get('/export', [MedicalProviderController::class, 'export'])->name('Md.export');
    Route::post('/', [MedicalProviderController::class, 'store'])->name('Md.store');
    Route::get('/{provider}', [MedicalProviderController::class, 'show']);
    Route::get('/{provider}/edit', [MedicalProviderController::class, 'edit'])->name('Md.edit');
    Route::put('/{provider}', [MedicalProviderController::class, 'update'])->name('Md.update');
    Route::delete('/{provider}', [MedicalProviderController::class, 'delete'])->name('Md.delete');
});

//Categories
Route::prefix('medical-departments')->middleware(['auth', 'role:medical'])->group(function () {
    Route::get('/', [MedicalProviderCategoryController::class, 'index'])->name('MdCat.index');
    Route::get('/create', [MedicalProviderCategoryController::class, 'create'])->name('MdCat.create');
    Route::post('/', [MedicalProviderCategoryController::class, 'store'])->name('MdCat.store');
    Route::get('/{cat}', [MedicalProviderCategoryController::class, 'show']);
    Route::get('/{cat}/edit', [MedicalProviderCategoryController::class, 'edit'])->name('MdCat.edit');
    Route::put('/{cat}', [MedicalProviderCategoryController::class, 'update'])->name('MdCat.update');
    Route::delete('/{cat}', [MedicalProviderCategoryController::class, 'delete'])->name('MdCat.delete');
});

//Attendance
//Route::prefix('attendance')->middleware(['auth', 'role:manager'])->group(function () {
//    Route::get('/', [AttendanceController::class, 'index']);
//    Route::get('/create', [AttendanceController::class, 'create']);
//    Route::post('/', [AttendanceController::class, 'store']);
//    Route::get('/{attendance}', [AttendanceController::class, 'show']);
//    Route::get('/{attendance}/edit', [AttendanceController::class, 'edit']);
//    Route::put('/{attendance}', [AttendanceController::class, 'update']);
//    Route::delete('/{attendance}', [AttendanceController::class, 'delete']);
//});

//Advance Requests
Route::prefix('advance-requests')->middleware(['auth', 'role:savings'])->group(function () {
    Route::get('/', [AdvanceRequestController::class, 'index']);
    Route::get('/create', [AdvanceRequestController::class, 'create']);
    Route::post('/upload', [AdvanceRequestController::class, 'upload']);
    Route::post('/', [AdvanceRequestController::class, 'store']);
    Route::get('/{req}', [AdvanceRequestController::class, 'show']);
    Route::get('/{req}/edit', [AdvanceRequestController::class, 'edit'])->name('SvF.edit');
    Route::put('/{req}', [AdvanceRequestController::class, 'update'])->name('SvF.update');
    Route::delete('/{req}', [AdvanceRequestController::class, 'delete'])->name('SvF.delete');
});

Route::prefix('ideas')->middleware(['auth', 'role:ideas'])->group(function () {
    Route::get('/', [IdeasController::class, 'index'])->name('ideas.index');
    Route::get('/export', [IdeasController::class, 'export'])->name('ideas.export');
    Route::get('/{idea}', [IdeasController::class, 'show'])->name('ideas.show');
});

//Authenticated Users
Route::prefix('profile')->middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect('/profile/overview');
    });
    Route::get('overview', [ProfilesController::class, 'show'])->name('profile.show');
    Route::get('edit', [ProfilesController::class, 'edit']);
    Route::post('update', [ProfilesController::class, 'update']);
});

Route::get('/login', [UsersController::class, 'showLogin'])->name('login');
Route::post('/login', [UsersController::class, 'doLogin']);
Route::post('signout', [UsersController::class, 'signout'])->name('signout');
