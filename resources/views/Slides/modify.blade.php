@extends('Template.main')

@section('page-title', 'New/edit Slide')


@section('content')
    <form action="{{ $route }}" method="post" enctype="multipart/form-data">
        @csrf
        @method($method)
        <div class="ml-8" style="max-width: 90%">
            @if ($errors->any())
                <div class="block mx-auto text-center my-2 alert alert-danger bg-red-400">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('message'))
                <div class="bg-green-100 px-4 py-4">
                    {{ session('message') }}
                </div>
            @endif
            <div class="flex mt-2 ml-2">
                <img src="{{ ($slide->link) ?? asset('images/default-image.png') }}" alt="" style="width: 480px; height: 192px; object-fit: contain">
            </div>

            <div class="flex flex-col mt-2 ml-2">
                <label for="image">Slide image (480 × 192px) </label>
                <input id="image" type="file" name="image">
            </div>

            <div class="flex mt-2 ml-2">
                <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                        shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Submit
                </button>
            </div>
        </div>
    </form>
@endsection
