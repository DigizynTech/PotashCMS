@extends('Template.main')

@section('page-title', 'New Announcement')


@section('content')
    <form action="{{ $route }}" method="post" enctype="multipart/form-data">
        @csrf
        @method($method)
        <div class="ml-8" style="max-width: 90%">
            @if ($errors->any())
                <div class="block mx-auto text-center my-2 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('message'))
                <div class="bg-green-100 px-4 py-4">
                    {{ session('message') }}
                </div>
            @endif
            <div class="flex">
                {{--                <div class="w-1/3 mr-2">--}}
                {{--                    <label for="language" class="block text-lg font-medium text-gray-700">Language</label>--}}
                {{--                    <select id="language" name="language"--}}
                {{--                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">--}}
                {{--                        <option {{ ($announcement->language == "english") ? "selected" : "" }}>English</option>--}}
                {{--                        <option {{ ($announcement->language == "arabic") ? "selected" : "" }}>Arabic</option>--}}
                {{--                    </select>--}}
                {{--                </div>--}}
                {{--                <div class="w-1/3 mr-2">--}}
                {{--                    <label for="status" class="block text-lg font-medium text-gray-700">Status</label>--}}
                {{--                    <select id="status" name="status"--}}
                {{--                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">--}}
                {{--                        <option {{ ($announcement->status == "published") ? "selected" : "" }}>Published</option>--}}
                {{--                        <option {{ ($announcement->status == "private") ? "selected" : "" }}>Private</option>--}}
                {{--                    </select>--}}
                {{--                </div>--}}

                {{--                <div class="w-1/3">--}}
                {{--                    <label for="date" class="block text-lg font-medium text-gray-700">Date</label>--}}
                {{--                    <div class="mt-1">--}}
                {{--                        <input type="date"--}}
                {{--                               name="date"--}}
                {{--                               id="date"--}}
                {{--                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"--}}
                {{--                               placeholder="2021-10-20"--}}
                {{--                               value="{{ $announcement->published_at->format("Y-m-d") }}"--}}
                {{--                        >--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
            <div class="flex mt-2">
                <div class="w-full mr-2 ">
                    <label for="title" class="block text-lg font-medium text-gray-700">Title</label>
                    <div class="mt-1">
                        <input type="text"
                               name="title"
                               id="title"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               value="{{ $announcement->title }}"
                        >
                    </div>
                </div>
            </div>
            <div class="flex mt-2 hidden">
                <div class="w-1/3 mr-2">
                    <label for="location" class="block text-lg font-medium text-gray-700">Department</label>
                    <select id="location" name="departmentId"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                        <option selected>All</option>
                        <option>D1</option>
                        <option>D2</option>
                    </select>
                </div>
                <div class="w-1/3">
                    <label for="location" class="block text-lg font-medium text-gray-700">Employees</label>
                    <select id="location" name="employeeId"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                        <option selected>All</option>
                        <option>E1</option>
                        <option>E2</option>
                    </select>
                </div>
            </div>
            <div class="flex mt-2">
                {{--                <div class="w-1/2 mr-2">--}}
                {{--                    <div class="brief_container">--}}
                {{--                        <input type="hidden" name="brief">--}}
                {{--                        <label for="Brief" class="block text-lg font-medium text-gray-700">Brief</label>--}}
                {{--                        <div id="Brief" class="">--}}
                {{--                            {!! $announcement->brief !!}--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="w-full">
                    <div class="content_container">
                        <input type="hidden" name="content">
                        <label for="content" class="block text-lg font-medium text-gray-700">Content</label>
                        <div id="content" class="">
                            {!! $announcement->content !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex mt-2 w-full">
                <div class="w-1/2">
                    <div class="flex">
                        <div>
                            @if($announcement->hasMedia('attachment'))
                                <button class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-indigo-500 hover:bg-indigo-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Preview
                                    attachment
                                </button>
                            @endif
                        </div>
                        <div class="pt-8">
                            <p class="text-md font-medium text-gray-700">Change attachment</p>
                            <input type="file" name="attachment">
                        </div>
                    </div>
                    <div class="pt-8">
                        <div>
                            <label>
                                Use link:
                                <input type="checkbox" name="hasTextLink" value="true">
                            </label>
                        </div>
                        <div class="pt-4">
                            <label>
                                Link text:
                                <input type="text" name="textLink" value="{{ $announcement->attachment }}">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="w-1/2 flex justify-end items-end mt-2">
                    <div>
                        <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                        shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Send
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection


@section('footer-scripts')
    <script>
        {{--document.getElementsByName('brief')[0].value = '{!! $announcement->brief !!}';--}}
        document.getElementsByName('content')[0].value = '{!! $announcement->content !!}';
        // var quill = new Quill('#Brief', {
        //     theme: 'snow',
        //     scrollingContainer: '.brief_container'
        // });
        //
        // quill.on('text-change', function (delta, oldDelta, source) {
        //     document.getElementsByName('brief')[0].value = quill.root.innerHTML;
        // });

        var quill2 = new Quill('#content', {
            theme: 'snow',
            scrollingContainer: '.content_container'
        });

        quill2.on('text-change', function (delta, oldDelta, source) {
            document.getElementsByName('content')[0].value = quill2.root.innerHTML;
        });
    </script>
@endsection
