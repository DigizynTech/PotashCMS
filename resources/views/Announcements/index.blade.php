@extends('Template.main')

@section('page-title', 'All Announcements')


@section('page-action')
    <a class="py-2 px-4 bg-primary-500 text-white rounded mr-4" href="/announcements/create">
        Add new announcement
    </a>
@endsection

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="flex flex-col" style="max-width: 90%; margin-left: 20px">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="shadow border-b border-gray-200 sm:rounded-lg">
            <table class="table-auto divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Title
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Status
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Language
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach( $announcements as $announcement )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $announcement->id }}
                        </td>
                        <td class="px-6 py-4 whitespace-normal">
                            {{ $announcement->title }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <x-tables.status :status=" $announcement->status == 'published' "/>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $announcement->language }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('Announcements.edit', $announcement->id) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <form action="{{ route('Announcements.delete', $announcement->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-600 hover:text-red-900" style="background: none">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                <!-- More people... -->
                </tbody>
            </table>
        </div>
    </div>

@endsection
