@extends('Template.main')

@section('page-title', 'Show user')


@section('content')
    <div class="ml-8" style="max-width: 90%">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="flex">
            <div class="w-1/4 mr-2">
                <label for="first_name" class="block text-lg font-medium text-gray-700">Job ID</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $user->job_id_number }}" readonly>
            </div>
            <div class="w-1/4 mr-2">
                <label for="status" class="block text-lg font-medium text-gray-700">Name</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $info['emp_name_arb'] ?? '' }}" readonly>
            </div>

            <div class="w-1/4 mr-2">
                <label for="date" class="block text-lg font-medium text-gray-700">Job Title</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $info['job_name'] ?? '' }}" readonly>

            </div>

            <div class="w-1/4 mr-2">
                <label for="date" class="block text-lg font-medium text-gray-700">Department</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $info['organization_name'] ?? '' }}" readonly>

            </div>
        </div>

        <div class="flex mt-4">
            <div class="w-1/4 mr-2">
                <label for="first_name" class="block text-lg font-medium text-gray-700">Medical Cuts</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $medical_info['MedicalDeductions'] ?? '' }}" readonly>
            </div>
            <div class="w-1/4 mr-2">
                <label for="status" class="block text-lg font-medium text-gray-700">Medical Visits</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $medical_info['MedicalVisits'] ?? '' }}" readonly>
            </div>

            <div class="w-1/4 mr-2">
                <label for="date" class="block text-lg font-medium text-gray-700">Vacation Balance</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $vacation_balance ?? '' }}" readonly>

            </div>

            <div class="w-1/4 mr-2 hidden">
                <label for="date" class="block text-lg font-medium text-gray-700">Savings Balance</label>
                <input type="text"
                       name="first_name"
                       id="first_name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md bg-gray-100"
                       value="{{ $savings ?? '' }}" readonly>
            </div>
        </div>

        <div class="flex mt-4">
            <form action="/users/moderation" method="post" class="flex w-3/4">
                @csrf
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <div class="w-1/3 mr-2">
                    <label for="date" class="block text-lg font-medium text-gray-700">New Password (Minimum 4 to change it)</label>
                    <input type="text"
                           name="password"
                           id="first_name"
                           class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                           value="">
                </div>
                <div class="w-1/3 mr-2 flex flex-col">
                    <label class="block text-lg font-medium text-gray-700">Privileges</label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="notifications"
                               style="display: inline-block;width: 20px;height: 20px;"
                            {{ in_array('notifications',explode(",",$user->role)) ? 'checked' : '' }}>
                        Manage Notifications
                    </label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="announcements"
                               {{ in_array('announcements',explode(",",$user->role)) ? 'checked' : '' }}
                               style="display: inline-block;width: 20px;height: 20px;">
                        Manage Announcements
                    </label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="medical"
                               {{ in_array('medical',explode(",",$user->role)) ? 'checked' : '' }}
                               style="display: inline-block;width: 20px;height: 20px;">
                        Manage Medical Provider
                    </label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="savings"
                               {{ in_array('savings',explode(",",$user->role)) ? 'checked' : '' }}
                               style="display: inline-block;width: 20px;height: 20px;">
                        Manage Saving Fund
                    </label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="ideas"
                               {{ in_array('ideas',explode(",",$user->role)) ? 'checked' : '' }}
                               style="display: inline-block;width: 20px;height: 20px;">
                        Manage Creative Ideas
                    </label>
                    <label>
                        <input type="checkbox"
                               name="privileges[]"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               value="users"
                               {{ in_array('users',explode(",",$user->role)) ? 'checked' : '' }}
                               style="display: inline-block;width: 20px;height: 20px;">
                        Manage users
                    </label>
                </div>
                <div class="w-1/3 mr-2 flex items-end">
                    <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Submit
                    </button>
                </div>
            </form>
        </div>
        <div class="flex mt-4 hidden">
            <div class="flex flex-col w-1/2 mr-4">
                <div class="shadow border-b border-gray-200 sm:rounded-lg">
                    <h3 class="text-2xl pl-4">Attendance</h3>
                    <table class="table-auto w-full divide-y divide-gray-200 ">
                        <thead class="bg-gray-50">
                        <tr>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Date
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                From
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                To
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Type
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach( $attendance as $t )
                            <tr>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['date'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['from'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['to'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['type'] }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="flex mt-4 hidden">
            <div class="flex flex-col w-1/2 mr-4">
                <div class="shadow border-b border-gray-200 sm:rounded-lg">
                    <h3 class="text-2xl pl-4">Vacations</h3>
                    <table class="table-auto w-full divide-y divide-gray-200 ">
                        <thead class="bg-gray-50">
                        <tr>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                From
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                To
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Type
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Interval
                            </th>
                            <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Reason
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach( $vacations as $t )
                            <tr>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['from'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['to'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['type'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['Interval'] }}
                                </td>
                                <td class="px-4 py-1 whitespace-nowrap">
                                    {{ $t['reason'] }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
