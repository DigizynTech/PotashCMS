@extends('Template.main')

@section('page-title', 'Create user')

@section('content')
    <div class="ml-8" style="max-width: 90%">
        @if ($errors->any())
            <div class="block mx-auto text-center my-2 alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/users/store" method="post" class="flex w-3/4">
            @csrf
            <div class="w-1/4 mr-2">
                <label for="jobid" class="block text-lg font-medium text-gray-700">Job id (if available)</label>
                <input type="text"
                       name="jobid"
                       id="jobid"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                       value="">
            </div>
            <div class="w-1/4 mr-2">
                <label for="name" class="block text-lg font-medium text-gray-700">Name</label>
                <input type="text"
                       name="name"
                       id="name"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                       value="">
            </div>
            <div class="w-1/4 mr-2">
                <label for="password" class="block text-lg font-medium text-gray-700">Password</label>
                <input type="password"
                       name="password"
                       id="password"
                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                       value="">
            </div>
            <div class="w-1/4 mr-2 flex items-end">
                <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Submit
                </button>
            </div>
        </form>
    </div>

@endsection
