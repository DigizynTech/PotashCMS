@extends('Template.main')

@section('page-title', 'Notification edit')


@section('content')
    <script>
        function apcInfo() {
            return {
                departments: JSON.parse('{!! json_encode($departments) !!}'),
                employees: JSON.parse('{!! json_encode($employees) !!}'),
                selected_dep: '{{ $article->departmentId }}',
                selected_emp: {{ $article->employeeId }},
                selected_emps: [],
                filterEmps() {
                    if (this.selected_dep === 'All') {
                        this.selected_emps = this.employees;
                        this.selected_emp = 0;
                    } else {
                        this.selected_emps = this.employees.filter(function (emp) {
                            return emp.department === this.selected_dep || emp.id === 0;
                        }.bind(this));
                    }
                }
            }
        }
    </script>
    <form action="{{ $route }}" method="post" x-data="apcInfo()" x-init="filterEmps">
        @csrf
        @method($method)
        <div class="ml-8" style="max-width: 60%">
            @if ($errors->any())
                <div class="block mx-auto text-center my-2 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('message'))
                <div class="bg-green-100 px-4 py-4">
                    {{ session('message') }}
                </div>
            @endif
            <div class="flex">
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Title</label>
                    <div class="mt-1">
                        <input type="text"
                               name="title"
                               id="title"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               maxlength="65"
                               value="{{ $article->title }}">
                    </div>
                </div>
            </div>

            <div class="flex mt-4">
                <div class="w-1/2 mr-2">
                    <label for="location" class="block text-lg font-medium text-gray-700">Department</label>
                    <select id="location" name="departmentId"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
                            x-model="selected_dep"
                            @change="filterEmps">
                        <template x-for="department in departments" :key="department">
                            <option :value="department" x-text="department" :selected="department === selected_dep"></option>
                        </template>
                    </select>
                </div>
                <div class="w-1/2">
                    <label for="location" class="block text-lg font-medium text-gray-700">Employees</label>
                    <select id="location" name="employeeId"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
                            x-model="selected_emp">
                        <template x-for="emp in selected_emps" :key="emp.id">
                            <option :value="emp.id" x-text="emp.name" :selected="emp.id === selected_emp"></option>
                        </template>
                    </select>
                </div>
            </div>

            <div class="content_container mt-2">
                <input type="hidden" name="content">
                <label for="content" class="block text-lg font-medium text-gray-700">Content</label>
                <div id="content" class="">
                    {!! $article->content !!}
                </div>
                <div id="content_message" class="text-black font-bold"></div>
            </div>
            <div class="content_container mt-2">
                <input type="hidden" name="fullContent">
                <label for="content" class="block text-lg font-medium text-gray-700">Full Content</label>
                <div id="fullContent" class="">
                    {!! $article->full !!}
                </div>
                <div id="content_message" class="text-black font-bold"></div>
            </div>
            <div class="flex justify-end mt-2">
                <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Submit
                </button>
            </div>

        </div>

    </form>


@endsection


@section('footer-scripts')
    <script>
        document.getElementsByName('content')[0].value = '{!! $article->content !!}';
        document.getElementsByName('fullContent')[0].value = '{!! $article->full !!}';

        if (document.getElementsByName('content')[0].value.length - 7 < 0) {
            length = 0;
        } else {
            length = document.getElementsByName('content')[0].value.length - 7;
        }
        document.getElementById('content_message').innerText = length + " of 110";

        var quill = new Quill('#fullContent', {
            theme: 'snow',
            scrollingContainer: '.content_container'
        });
        quill.on('text-change', function (delta, oldDelta, source) {
            document.getElementsByName('fullContent')[0].value = quill.root.innerHTML;
        });
        var quill2 = new Quill('#content', {
            theme: 'snow',
            scrollingContainer: '.content_container'
        });
        quill2.on('text-change', function (delta, oldDelta, source) {
            document.getElementsByName('content')[0].value = quill2.root.innerHTML;
            if (document.getElementsByName('content')[0].value.length - 7 < 0) {
                length = 0;
            } else {
                length = document.getElementsByName('content')[0].value.length - 7;
            }
            document.getElementById('content_message').innerText = length + " of 110";
        });
    </script>
@endsection
