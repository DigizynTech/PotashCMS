@extends('Template.main')

@section('page-title', 'All Notifications')


@section('page-action')
    <a class="py-2 px-4 bg-primary-500 text-white rounded" href="/news/create">
        Add new Notification
    </a>
@endsection

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->

    <div class="flex flex-col" style="max-width: 90%; margin-left: 20px">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="shadow border-b border-gray-200 sm:rounded-lg">
            <table class="table-auto w-full divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Title
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Department
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Employee
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Push to users
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Sent?
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Remove</span>
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach( $articles as $article )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $article->id }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $article->title }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $article->departmentId }}
                        </td>
                        <td class="px-6 py-4">
                            {{ ($article->employeeId == 0) ? 'All' : \App\Models\User::where('id',$article->employeeId)->first()->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <form action="{{ route('News.push', $article->id) }}" method="post">
                                @csrf
                                <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Push Notification
                                </button>
                            </form>
                        </td>
                        <td class="px-6 py-4">
                            {{ ($article->sent) ? "SENT" : "NOT SENT" }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('News.modify', $article->id) }}" class="text-indigo-600 hover:text-indigo-900">
                                Edit
                            </a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <form action="{{ route('News.delete', $article->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-600 hover:text-red-900" style="background: none">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
