<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        h1 {
            font-size: 18px;
        }
        p {
            line-height: 1;
            font-size: 16px;
        }
    </style>
</head>
<body>

<h1>New Creative Idea from {{ $user->fullname }},</h1>
<p>Name: {{ $user->fullname }}</p>
<p>Department: {{ $user->department }}</p>
<p>Title: {{ $idea->title }}</p>
<p>Categories: {{ $categories }}</p>
<p>Description: {{ $idea->description }}</p>
<p>Tools: {{ $idea->tools }}</p>
<p> Steps: {{ $idea->steps }}</p>
<p> Date {{ \Illuminate\Support\Carbon::now()->format('d/m/Y') }}</p>


<p>Best,</p>
<p>Arab Potash CMS Mobile System</p>
</body>
</html>
