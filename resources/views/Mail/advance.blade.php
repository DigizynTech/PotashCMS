<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        h1 {
            font-size: 18px;
        }
        p {
            line-height: 1;
            font-size: 16px;
        }
    </style>
</head>
<body>

<h1>New Advance Request from {{ $user->fullname }}</h1>

<p>Job ID: {{ $user->job_id_number }}</p>
<p>Name: {{ $user->fullname }}</p>
<p>Position: {{ $user->job_title }}</p>
<p>Department: {{ $user->department }}</p>
<p> Amount: {{ $amount }}</p>
<p> Period: {{ $period }}</p>
<p> Iban: {{ $iban }}</p>

<p>Best,</p>
<p>Arab Potash CMS Mobile System</p>
</body>
</html>
