@extends('Template.main')

@section('page-title', 'All Creative Ideas')


@section('page-action')
    <form action="{{ route('ideas.export') }}" class="flex gap-2">
        <label for="start">Start Date
            <input type="date" name="start_date" id="start"></label>
        <label for="start">End Date
            <input type="date" name="end_date" id="start"></label>
        <button class="inline-flex items-center ml-4 px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" >
            Export All ideas
        </button>
    </form>

@endsection

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->

    <div class="flex flex-col " style="max-width: 90%; margin-left: 20px">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="shadow border-b border-gray-200 sm:rounded-lg w-1/2 mx-auto">
            <table class="table-auto w-full divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Title
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Created By
                    </th>
                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">

                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach( $ideas as $idea )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $idea->id }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $idea->title }}
                        </td>
                       <td class="px-6 py-4">
                           {{ $idea->user->fullname ?? $idea->created_by }}
                       </td>
                        <td class="px-6 py-4">
                            <a href="{{ route('ideas.show', $idea->id) }}">Show</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
