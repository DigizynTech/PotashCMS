@extends('Template.main')

@section('page-title', 'Show Idea #'.$idea->id)


@section('content')
    <form action="" method="post">
        <div class="ml-8" style="max-width: 60%">
            <div class="flex">
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Title اسم الفكرة</label>
                    <div class="mt-1">
                        <input type="text"
                               name="title"
                               id="title"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               maxlength="65"
                               value="{{ $idea->title }}">
                    </div>
                </div>
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Categories محور الفكرة المقترحة</label>
                    <div class="mt-1">
                        <textarea rows="4" name="comment" id="title"
                                  class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300
                                   rounded-md">{!! $categories !!}</textarea>
                    </div>
                </div>
            </div>
            <div class="flex">
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Description وصف الفكرة التحسينية</label>
                    <div class="mt-1">
                        <textarea rows="4" name="comment" id="title"
                                  class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full
                                   sm:text-sm border-gray-300 rounded-md">{{ $idea->description }}</textarea>
                    </div>
                </div>
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Tools المعدات المطلوبة</label>
                    <div class="mt-1">
                        <textarea rows="4" name="comment" id="title"
                                  class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300
                                   rounded-md">{!! $idea->tools !!}</textarea>
                    </div>
                </div>
            </div>
            <div class="flex">
                <div class="w-1/2 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Steps المراحل التحسينية للتطبيق</label>
                    <div class="mt-1">
                        <textarea rows="4" name="comment" id="title"
                                  class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300
                                  rounded-md">{{ $idea->steps }}</textarea>

                    </div>
                </div>
            </div>
            <div class="flex">
                <div class="w-1/3 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">User اسم صاحب الفكرة</label>
                    <div class="mt-1">
                        <input type="text"
                               name="title"
                               id="title"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               maxlength="65"
                               value="{{ $idea->user->fullname ?? $idea->created_by }}">

                    </div>
                </div>
                <div class="w-1/3 mr-2 mt-2">
                    <label for="title" class="block text-lg font-medium text-gray-700">Date تاريخ إرسال الفكرة</label>
                    <div class="mt-1">
                        <input type="text"
                               name="title"
                               id="title"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               maxlength="65"
                               value="{{ $idea->created_at->format('d/m/Y') }}">

                    </div>
                </div>
            </div>

        </div>

    </form>

@endsection
