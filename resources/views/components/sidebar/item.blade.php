<div>
    <a href="{{ $link }}" class="bg-secondary-500 text-white group flex items-center mt-2 px-2 py-2 text-sm font-medium rounded-md">
        <span class="material-icons" style="margin-right: 10px;">{{ $icon }}</span>
        {{ $name }}
    </a>
</div>
