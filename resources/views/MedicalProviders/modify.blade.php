@extends('Template.main')

@section('page-title', 'New Medical Provider')


@section('content')
    <script>
        function medicalCategories() {
            return {
                categories: JSON.parse('{!! json_encode($categories) !!}'),
                subCategories: JSON.parse('{!! json_encode($subCategories) !!}'),
                selected_cat: {!! $selected_cat !!},
                selected_sub_cat: {!! $selected_sub_cat !!},
                filtered_sub_cat: [],
                filterCategories() {
                    this.filtered_sub_cat = this.subCategories.filter(function (cat) {
                        return parseInt(cat.parent) === parseInt(this.selected_cat);
                    }.bind(this));
                    if (this.filtered_sub_cat.length === 0) {
                        this.filtered_sub_cat = [{id: this.selected_cat, name: '--', parent: this.selected_cat}]
                    }
                }
            }
        }
    </script>
    <form action="{{ $route }}" method="post">
        @csrf
        @method($method)
        <div class="ml-8" style="max-width: 90%" x-data="medicalCategories()" x-init="filterCategories()">
            @if(session('message'))
                <div class="bg-green-100 px-4 py-4">
                    {{ session('message') }}
                </div>
            @endif
            <div class="flex mt-2">
                <div class="w-1/3 mr-2 ">
                    <label for="name" class="block text-lg font-medium text-gray-700">Name</label>
                    <div class="mt-1">
                        <input type="text"
                               name="name"
                               id="name"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               value="{{ $provider->name }}">
                    </div>
                </div>
                <div class="w-1/3 mr-2 ">
                    <label for="name_ar" class="block text-lg font-medium text-gray-700">Name (Arabic)</label>
                    <div class="mt-1">
                        <input type="text"
                               name="name_ar"
                               id="name_ar"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here"
                               value="{{ $provider->name_ar }}">
                    </div>
                </div>
            </div>
            <div class="flex mt-2">
                <div class="w-1/3 mr-2 ">
                    <label for="address" class="block text-lg font-medium text-gray-700">Address</label>
                    <div class="mt-1">
                        <input type="text"
                               name="address"
                               id="address"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ $provider->address }}">
                    </div>
                </div>
                <div class="w-1/3 mr-2 ">
                    <label for="address_ar" class="block text-lg font-medium text-gray-700">Address (Ar)</label>
                    <div class="mt-1">
                        <input type="text"
                               name="address_ar"
                               id="address_ar"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ $provider->address_ar }}">
                    </div>
                </div>
            </div>
            <div class="flex mt-2">
                <div class="w-1/3 mr-2 ">
                    <label for="phone" class="block text-lg font-medium text-gray-700">Phone</label>
                    <div class="mt-1">
                        <input type="text"
                               name="phone"
                               id="phone"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ $provider->phone }}">
                    </div>
                </div>
                <div class="w-1/3 mr-2 ">
                    <label for="phone_2" class="block text-lg font-medium text-gray-700">Secondary Phone</label>
                    <div class="mt-1">
                        <input type="text"
                               name="phone_2"
                               id="phone_2"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ $provider->phone_2 }}">
                    </div>
                </div>
            </div>
            <div class="flex mt-2">
                <div class="w-1/3 mr-2 ">
                    <label for="category" class="block text-lg font-medium text-gray-700">Category</label>
                    <select id="category" name="category" x-model="selected_cat" @change="filterCategories()"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                        <template x-for="cat in categories" :key="cat.id">
                            <option :value="cat.id" x-text="cat.name" :selected="cat.id === selected_cat"></option>
                        </template>
                    </select>
                </div>
                <div class="w-1/3 mr-2 ">
                    <label for="category" class="block text-lg font-medium text-gray-700">Sub Category</label>
                    <select id="category" name="sub_category" x-model="selected_sub_cat"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                        <template x-for="cat in filtered_sub_cat" :key="cat.id">
                            <option :value="cat.id" x-text="cat.name" :selected="cat.id === selected_sub_cat"></option>
                        </template>
                    </select>
                </div>
            </div>

            <div class="flex mt-2">
                <div class="w-1/3 mr-2 ">
                    <label for="city" class="block text-lg font-medium text-gray-700">City</label>
                    <select id="city" name="city"
                            class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                        @foreach( $cities as $key => $value )
                            <option value="{{ $value }}" {{ ($value == $provider->city) ? 'selected' : '' }}>{{ $key }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="w-1/3  flex justify-end items-end mt-2">
                    <div>
                        <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                        shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('footer-scripts')
@endsection
