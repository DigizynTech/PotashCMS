@extends('Template.main')

@section('page-title', 'All Medical Providers')


@section('page-action')
    <a class="py-2 px-4 bg-primary-500 text-white rounded mr-4" href="/medical-providers/create">
        Add new Medical Provider
    </a>
    <a class="py-2 px-4 bg-secondary-500 text-white rounded mr-4" href="/medical-departments/">
        Manage medical providers categories
    </a>
    <a class="py-2 px-4 bg-secondary-500 text-white rounded" href="/medical-providers/export">
        Export All Medical providers
    </a>
@endsection

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="flex flex-col" style="max-width: 90%; margin-left: 20px">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <script>
            function medicalCategories() {
                return {
                    categories: JSON.parse('{!! json_encode($categories) !!}'),
                    subCategories: JSON.parse('{!! json_encode($subCategories) !!}'),
                    selected_cat: {!! $selected_cat !!},
                    selected_sub_cat: {!! $selected_sub_cat !!},
                    filtered_sub_cat: [],
                    filterCategories() {
                        this.filtered_sub_cat = this.subCategories.filter(function (cat) {
                            return parseInt(cat.parent) === parseInt(this.selected_cat);
                        }.bind(this));
                        if (this.filtered_sub_cat.length === 0) {
                            this.filtered_sub_cat = [{id: this.selected_cat, name: '--', parent: this.selected_cat}]
                        }
                    }
                }
            }
        </script>
        <form action="" method="GET" x-data="medicalCategories()" x-init="filterCategories()" class="mb-4">
            <div class="flex mt-2">
                <div class="w-1/4 mt-2 mr-2 ">
                    <label for="search" class="block text-lg font-medium text-gray-700">Name</label>
                    <div class="mt-1">
                        <input type="text"
                               name="search"
                               id="search"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ request('search') }}">
                    </div>
                </div>
                <div class="w-1/4 mt-2 mr-2">
                    <div>
                        <label for="month" class="block text-lg font-medium text-gray-700">Category </label>
                        <div class="mt-1">
                            <select id="category" name="category" x-model="selected_cat" @change="filterCategories()"
                                    class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                <template x-for="cat in categories" :key="cat.id">
                                    <option :value="cat.id" x-text="cat.name" :selected="cat.id === selected_cat"></option>
                                </template>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="w-1/4 mt-2 mr-2">
                    <div>
                        <label for="month" class="block text-lg font-medium text-gray-700">Sub Category </label>
                        <div class="mt-1">
                            <select id="category" name="sub_category" x-model="selected_sub_cat"
                                    class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                <template x-for="cat in filtered_sub_cat" :key="cat.id">
                                    <option :value="cat.id" x-text="cat.name" :selected="cat.id === selected_sub_cat"></option>
                                </template>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="w-1/4 mt-2 mr-2">
                    <div>
                        <label for="city" class="block text-lg font-medium text-gray-700">City: </label>
                        <div class="mt-1">
                            <select id="city" name="city"
                                    class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                @foreach($cities as $key => $value)
                                    <option value="{{ $value }}" {{ ((int)request('city') == (int)$value) ? 'selected' : '' }}>{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="w-1/4 mt-2 flex justify-start items-end ">
                    <div>
                        <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                        shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Submit
                        </button>
                    </div>
                </div>
            </div>

        </form>
        <div class="shadow border-b border-gray-200 sm:rounded-lg">
            <table class="table-auto min-w-full divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id ({{ $providers->count() }})
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        name
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Phone
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Category
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        City
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Delete</span>
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @php
                    $cities_names = array_keys($cities);
                @endphp
                @foreach( $providers as $provider )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $provider->id }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $provider->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $provider->phone }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $provider->MedicalCategory->name }}
{{--                            {{ ($provider->MedicalCategory->parentCat() != '') ? ", " . $provider->MedicalCategory->parentCat() : "" }}--}}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $cities_names[$provider->city] }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('Md.edit', $provider->id) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <form action="{{ route('Md.delete', $provider->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-600 hover:text-red-900" style="background: none">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                <!-- More people... -->
                </tbody>
            </table>
        </div>
    </div>

@endsection
