@extends('Template.main')


@section('page-title', 'Modify saving fund data for #'. $req->user_id . ' at ' . \Carbon\Carbon::parse($req->date)->format('M-Y') )


@section('page-action')

@endsection


@section('content')
    <div class="ml-8" style="max-width: 90%">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="flex mt-2" class="w-full">
            <form action="{{ $route }}" method="post" class="w-full">
                @csrf
                @method($method)
                <div class="flex mt-2 w-full">
                    <div class="w-1/4 mr-2  ">
                        <label for="saving_account_balance" class="block text-lg font-medium text-gray-700">Saving Fund Balance</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="saving_account_balance"
                                   id="saving_account_balance"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Saving Fund Balance" value="{{ $req->saving_account_balance }}">
                        </div>
                    </div>
                    <div class="w-1/4 mr-2 ">
                        <label for="payment_period" class="block text-lg font-medium text-gray-700">Payment Period</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="payment_period"
                                   id="payment_period"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Payment Period" value="{{ $req->payment_period }}">
                        </div>
                    </div>
                    <div class="w-1/4 mr-2">
                        <label for="request_amount" class="block text-lg font-medium text-gray-700">Requested amount</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="request_amount"
                                   id="request_amount"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Payment Period" value="{{ $req->request_amount }}">
                        </div>
                    </div>
                    <div class="w-1/4 mr-2">
                        <label for="remaining_amount" class="block text-lg font-medium text-gray-700">Remaining Amount</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="remaining_amount"
                                   id="remaining_amount"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Payment Period" value="{{ $req->remaining_amount }}">
                        </div>
                    </div>
                </div>
                <div class="flex mt-4">
                    <div class="w-full flex justify-end items-end mt-2">
                        <div>
                            <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
