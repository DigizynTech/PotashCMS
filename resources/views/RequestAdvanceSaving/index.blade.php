@extends('Template.main')


@section('page-title', 'Saving funds for '. $month)


@section('page-action')
    @if ( \Illuminate\Support\Str::length(request('jobId')) > 0)
        <span>This page is filter for Job id: {{ request('jobId') }}</span>
    @endif
@endsection

@section('content')
    <div class="flex flex-col mt-4" style="max-width: 90%; margin-left: 20px">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif

        <form action="" method="GET">
            <div class="flex mt-2 hidden">
                <div class="w-1/3 mt-2 mr-2 ">
                    <label for="jobId" class="block text-lg font-medium text-gray-700">Job ID</label>
                    <div class="mt-1">
                        <input type="text"
                               name="jobId"
                               id="jobId"
                               class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                               placeholder="Enter the article title here" value="{{ request('jobId') }}">
                    </div>
                </div>
                <div class="w-1/3 mt-2 mr-2">
                    <div>
                        <label for="month" class="block text-lg font-medium text-gray-700">Month </label>
                        <div class="mt-1">
                            <select id="month" name="month"
                                    class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                <option value="any">Any</option>
                                @foreach( $months as $month)
                                    <option value="{{$month}}" {{ (request('month') == $month) ? 'selected' : '' }}>{{ $month }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="w-1/3 mt-2 flex justify-start items-end ">
                    <div>
                        <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                        shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Submit
                        </button>
                    </div>
                </div>
            </div>

        </form>
        <form action="/advance-requests/upload" method="post" enctype="multipart/form-data">
            @csrf
            <h3 class="font-bold mt-4" style="font-size: 20px;">Bulk upload:</h3>
            <div class="flex mt-2">
                <div class="w-1/4 mr-2 ">
                    <label for="name" class="block text-lg font-medium text-gray-700">CSV FILE: </label>
                    <div class="mt-1">
                        <input type="file"
                               name="upload"
                               id="upload">
                    </div>
                </div>
                <div class="w-1/4 mr-2">
                    <label for="month" class="block text-lg font-medium text-gray-700">Month </label>
                    <div class="mt-1">
                        <select id="month" name="month"
                                class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                            @foreach( $months as $month)
                                <option value="{{$month}}">{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="w-1/4 flex justify-start items-end">
                    <div>
                        <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Submit
                        </button>
                    </div>
                </div>
                <div class="w-1/4 flex justify-start items-end">
                    <div>
                        <a href="/storage/3/advance_requests_sample.csv">
                            <button type="button" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Download CSV Template
                            </button>
                        </a>
                    </div>
                </div>
            </div>

        </form>
        <div class="shadow border-b border-gray-200 sm:rounded-lg mt-8 hidden">
            <table class="w-full table-auto divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Name
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Saving Balance
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Request Amount
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Payment Period
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Remaining Account
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Edit
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Delete
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach($rows as $row)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $row->user_id }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $row->saving_account_balance }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $row->request_amount }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $row->payment_period }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $row->remaining_amount }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ \Carbon\Carbon::parse($row->date)->format('M-Y') }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('SvF.edit', $row->id) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <form action="{{ route('SvF.delete', $row->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-600 hover:text-red-900" style="background: none">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
