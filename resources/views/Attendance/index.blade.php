@extends('Template.main')


@section('page-title', 'Attendance Report')


@section('page-action')

@endsection

@section('content')
    <div class="flex" style="max-width: 90%; margin-left: 20px">
        <form class="w-full" action="/attendance" method="get">
            <div class="flex w-full">
                <div class="w-1/4">
                    <label for="date" class="block text-lg font-medium text-gray-700">Select Date</label>
                    <input class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                           type="date" name="date" id="date" value="{{ $date }}">
                </div>
            </div>
            <div class="flex mt-2">
                <div class="w-1/2">
                    <input type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" value='Search'>

                </div>
            </div>
        </form>
    </div>
    <div class="flex flex-col" style="max-width: 90%; margin-left: 20px">

        <div class="shadow border-b border-gray-200 sm:rounded-lg">
            <table class="table-auto divide-y divide-gray-200 mt-4 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Name
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Attend Time
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Leave Time
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach( $attendance as $attendee )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $attendee->id }}
                        </td>
                        <td class="px-6 py-4 whitespace-normal">
                            {{ $attendee->user->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-normal">
                            {{ $attendee->date }}
                        </td>
                        <td class="px-6 py-4 whitespace-normal">
                            {{ $attendee->attendance_time }}
                        </td>
                        <td class="px-6 py-4 whitespace-normal">
                            {{ $attendee->leave_time }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                        </td>
                    </tr>
                @endforeach

                <!-- More people... -->
                </tbody>
            </table>
        </div>
    </div>

@endsection
