<div class="hidden md:flex md:w-64 md:flex-col md:fixed md:inset-y-0">
    <div class="flex-1 flex flex-col min-h-0 bg-gray-800">
        <div class="flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
            <div class="flex items-center flex-shrink-0 px-4">
                <img class="h-12 w-auto" src="{{ asset('images/arab_potash-logo-ar-white.png') }}" alt="Potash Logo">
            </div>
            <nav class="mt-5 flex-1 px-2 space-y-1">
                <x-sidebar.item name="Dashboard" link="/" icon="home"/>
                @hasrole('notifications')
                <x-sidebar.item name="Notification" link="/news" icon="article"/>
                @endhasrole
                @hasrole('announcements')
                <x-sidebar.item name="Announcements" link="/announcements" icon="campaign"/>
                @endhasrole
                @hasrole('medical')
                <x-sidebar.item name="Medical Providers" link="/medical-providers" icon="medical_services"/>
                @endhasrole
                @hasrole('ideas')
                <x-sidebar.item name="Ideas" link="{{ route('ideas.index') }}" icon="lightbulb"/>
                @endhasrole
                @hasrole('savings')
                <x-sidebar.item name="Saving Fund" link="/advance-requests" icon="payments"/>
                @endhasrole
                @hasrole('announcements')
                <x-sidebar.item name="Home screen slides" link="/slides" icon="perm_media"/>
                @endhasrole

                @hasrole('users')
                <x-sidebar.item name="Users" link="/users" icon="account_circle"/>
                @endhasrole
            </nav>
        </div>
        <div class="flex-shrink-0 flex bg-black p-4">
            <div class="flex items-center">
                <div>
                    <img class="inline-block h-9 w-9 rounded-full"
                         src="{{ $profileImage ?? asset('images/male-vector.png') }}"
                         alt="">
                </div>
                <div class="ml-3">
                    <p class="text-sm font-medium text-white">
                        {{ Str::ucfirst(Auth::user()->name) }}
                    </p>
                    <form action="{{ route('signout') }}" method="post">
                        @csrf
                        <input
                            class="text-xs font-medium text-gray-300 group-hover:text-gray-200 cursor-pointer"
                            style="background: none"
                            type="submit" value="Sign out">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
