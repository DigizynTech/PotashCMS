<!doctype html>
<html lang="en" class="h-full bg-gray-100">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('page-title', $pageTitle ?? 'Default Page Name') - PotashCMS
    </title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendors/quill/quill.snow.css') }}">
    <script src="//unpkg.com/alpinejs" defer></script>

    @livewireStyles

</head>
<body class="h-full">
<div>
    <header>
        @include('Template.header')
    </header>

    <div>
        <aside>
            @include('Template.sidebar')
        </aside>
        <div class="md:pl-64 flex flex-col flex-1">
            <main class="flex-1">
                <div class="py-6">
                    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                        <h1 class="text-2xl font-semibold text-gray-900">
                            @yield('page-title', $pageTitle ?? 'Default Page Name')
                            <span class="text-gray-700 text-sm">@yield('page-action','')</span>
                        </h1>

                    </div>
                </div>
                <div>
                    @yield('content')

                </div>
            </main>
        </div>
    </div>

    <footer>
        @include('Template.footer')
    </footer>
</div>
@livewireScripts

<script src="{{ asset('vendors/quill/quill.min.js') }}"></script>

@yield('footer-scripts')

</body>
</html>
