@extends('Template.main')

@section('page-title', 'Dashboard')

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div>

        <dl class="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-3 mx-4">
            <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">

                <dt class="text-sm font-medium text-gray-500 truncate">
                    <span class="material-icons" style="margin-right: 10px;vertical-align: bottom;">people</span>
                    Total Registered Users
                </dt>
                <dd class="mt-1 text-3xl font-semibold text-gray-900">
                    {{ $data['users'] }}
                </dd>
            </div>

            <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
                <dt class="text-sm font-medium text-gray-500 truncate">
                    <span class="material-icons" style="margin-right: 10px;vertical-align: bottom;">smartphone</span>
                    Total Android Users
                </dt>
                <dd class="mt-1 text-3xl font-semibold text-gray-900">
                    {{ $data['android'] }}
                </dd>
            </div>

            <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
                <dt class="text-sm font-medium text-gray-500 truncate">
                    <span class="material-icons" style="margin-right: 10px;vertical-align: bottom;">phone_iphone</span>
                    Total iOS Users
                </dt>
                <dd class="mt-1 text-3xl font-semibold text-gray-900">
                    {{ $data['ios'] }}
                </dd>
            </div>
        </dl>
    </div>

@endsection
