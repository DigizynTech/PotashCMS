@extends('Template.main')

@section('page-title', 'Manage Medical providers categories')

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="flex flex-col" style="max-width: 90%; margin-left: 20px;margin-bottom: 40px">
        <form action="{{route('MdCat.store')}}" method="post" class="mb-4">
            @csrf
            <div class="ml-8" style="max-width: 90%">
                @if(session('message'))
                    <div class="bg-green-100 px-4 py-4">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="flex mt-2">
                    <div class="w-1/3 mr-2 ">
                        <label for="name" class="block text-lg font-medium text-gray-700">Category name</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="name"
                                   id="name"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Enter the new category name here">
                        </div>
                    </div>
                    <div class="w-1/3 mr-2 ">
                        <label for="name" class="block text-lg font-medium text-gray-700">Category name (Ar)</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="name_ar"
                                   id="name"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Enter the new category name here">
                        </div>
                    </div>
                    <div class="w-1/3  flex justify-end items-end mt-2">
                        <div>
                            <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <div class="shadow border-b border-gray-200 sm:rounded-lg">
            <table class="table-auto min-w-full divide-y divide-gray-200 ">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Name
                    </th>
                    <th scope="col" class="px-6 py-3 text-left  text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Name (Ar)
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Remove</span>
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach( $categories as $category )
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $category->id }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $category->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $category->name_ar }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('MdCat.index', ['parent'=>$category->id]) }}" class="text-indigo-600 hover:text-indigo-900">
                                Sub Categories
                            </a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{ route('MdCat.edit', $category->id) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <form action="{{ route('MdCat.delete', $category->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="text-red-600 hover:text-red-900" style="background: none">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
