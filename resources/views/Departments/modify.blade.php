@extends('Template.main')

@section('page-title', 'Edit category')


@section('content')
    <div class="ml-8" style="max-width: 90%">
        @if(session('message'))
            <div class="bg-green-100 px-4 py-4">
                {{ session('message') }}
            </div>
        @endif
        <div class="flex mt-2" class="w-full">
            <form action="{{ route('MdCat.update', $category->id ) }}" method="post" class="w-full">
                @csrf
                @method('PUT')
                <div class="flex mt-2 w-full">
                    <div class="w-1/3 mr-2  ">
                        <label for="name" class="block text-lg font-medium text-gray-700">Category name</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="name"
                                   id="name"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Enter the Department name" value="{{ $category->name }}">
                        </div>
                    </div>
                    <div class="w-1/3 mr-2  ">
                        <label for="name" class="block text-lg font-medium text-gray-700">Category name (ar)</label>
                        <div class="mt-1">
                            <input type="text"
                                   name="name_ar"
                                   id="name"
                                   class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                   placeholder="Enter the Department name" value="{{ $category->name_ar }}">
                        </div>
                    </div>
                    <div class="w-1/3  flex justify-end items-end mt-2">
                        <div>
                            <button type="submit" class="inline-flex items-center px-8 py-1 border border-transparent text-lg font-medium rounded-md
                            shadow-sm
        text-white bg-primary-500 hover:bg-tertiary-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
