<!doctype html>
<html lang="en" class="h-full bg-gray-100">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('page-title', $pageTitle ?? 'Default Page Name') - PotashCMS
    </title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendors/quill/quill.snow.css') }}">
</head>
<body class="overflow-hidden">
    <div class="w-full h-screen flex items-center justify-center">

        <form class="w-full md:w-1/3 bg-white rounded-lg" action="/login" method="post" autocomplete="off">
            @csrf
            <div class="flex font-bold justify-center mt-6">
                <img class="w-64"
                     src="{{ asset('images/arab_potash-logo-ar.png') }}" alt="PotashLogo">
            </div>
            <h2 class="text-3xl text-center text-gray-700 mb-4 mt-4">Login</h2>
            @if ($errors->any())
                <div class="block mx-auto text-center my-2 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="px-12 pb-10">
                <div class="w-full mb-2">
                    <div class="flex items-center">
                        <i class='ml-3 fill-current text-gray-400 text-xs z-10 fas fa-user'></i>
                        <input type='text' placeholder="Username"
                               class="-mx-6 px-8  w-full border rounded px-3 py-2 text-gray-700 focus:outline-none"
                                name="email"
                            />
                    </div>
                </div>
                <div class="w-full mb-2">
                    <div class="flex items-center">
                        <i class='ml-3 fill-current text-gray-400 text-xs z-10 fas fa-lock'></i>
                        <input type='password' placeholder="Password" autocomplete="new-password"
                               class="-mx-6 px-8 w-full border rounded px-3 py-2 text-gray-700 focus:outline-none"
                        name="password"/>
                    </div>
                </div>
                <button type="submit"
                        class="-mx-3 mt-2 w-full py-2 rounded-full bg-primary-500 text-gray-100  focus:outline-none">Login</button>
            </div>
        </form>
    </div>
</body>>
