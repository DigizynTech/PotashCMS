module.exports = {
    mode: 'jit',
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './app/**/*.php'
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {

        extend: {
            colors: {
                primary: {
                    500: '#00abbd'
                },
                secondary: {
                    500: '#50b848'
                },
                tertiary: {
                    500 : '#884c28'
                }
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
