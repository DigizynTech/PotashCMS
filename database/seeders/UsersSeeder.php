<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserData;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'password' => \Hash::make('P@ssw0rder75$'),
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'job_title' => 'admin',
            'job_id_number' => '001',
            'department' => 'admin',
            'mobile_number' => '00191',
            'accepted' => true,
            'accepted_by' => 1,
        ]);
        $user_id = \App\Models\User::where('email', 'LIKE','admin@admin.com')->first()->id;
        \App\Models\UserData::create([
            'user_id' => $user_id,
            'date' => '2021-09-01',
            'medical_cuts' => 12,
            'visits_count' => 12,
            'leaves_count' => 12,
            'saving_funds_balance' => 150
        ]);

        \App\Models\UserData::create([
            'user_id' => $user_id,
            'date' => '2021-10-01',
            'medical_cuts' => 15,
            'visits_count' => 17,
            'leaves_count' => 22,
            'saving_funds_balance' => 175
        ]);

        \App\Models\UserData::create([
            'user_id' => $user_id,
            'date' => '2021-11-01',
            'medical_cuts' => 12,
            'visits_count' => 12,
            'leaves_count' => 12,
            'saving_funds_balance' => 200
        ]);

        \App\Models\UserData::create([
            'user_id' => $user_id,
            'date' => '2021-12-01',
            'medical_cuts' => 12,
            'visits_count' => 12,
            'leaves_count' => 12,
            'saving_funds_balance' => 225
        ]);
        $user = \App\Models\User::where('email', 'LIKE','admin@admin.com')->first();
        $user->update([
            'password' => \Hash::make('P@ssw0rder75$')
        ]);

    }
}
