<?php

namespace Database\Seeders;

use App\Models\MedicalProviderCategory;
use Illuminate\Database\Seeder;

class MedicalCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ["Doctor", "أطباء", "0"],
            ["Dentist", "طب الاسنان", "0"],
            ["Physiotherapy", "علاج طبيعي", "0"],
            ["Optics Centers", "مراكز بصريات", "0"],
            ["Pharmacies", "الصـيدليـات", "0"],
            ["Laboratories", "المختبرات", "0"],
            ["X-Rays ", "مراكز الأشعة", "0"],
            ["Hospitals", "المستشفيات", "0"],
            ["Specialized Centers", "مراكز متخصصة", "0"],
            ["General Medicine", "الطب العام", "1"],
            ["General Surgery", "الجراحة العامة", "1"],
            ["Internal Medicine", "الامراض الباطنية", "1"],
            ["Endocrinologist", "أمراض السكري والغدد الصماء", "1"],
            ["Communicable diseases and infectious", "أمراض سارية ومعدية", "1"],
            ["Gastroenterologist", "امراض الجهاز الهضمي", "1"],
            ["Nephrology", "أمراض الكلى", "1"],
            ["Urology Surgery ", "جراحة المسالك البولية", "1"],
            ["Ear, Nose and Throat", "أنف وأذن وحنجرة", "1"],
            ["Pulmonologist", "الامراض الصدرية", "1"],
            ["Hematologist", "أمراض أورام ودم", "1"],
            ["Cardiology", "أمراض القلب والشرايين", "1"],
            ["Neurology ", "أمراض أعصاب ودماغ", "1"],
            ["Neurosurgery", "جراحة الأعصاب والدماغ", "1"],
            ["Orthopedic Surgeon ", "جراحة العظام والمفاصل", "1"],
            ["Rheumatology", "أمراض الروماتيزم", "1"],
            ["Vascular Surgery ", "جراحة أوعية دموية", "1"],
            ["Burn and Plastic Surgery", "جراحة الترميم والحروق", "1"],
            ["Gynecology & Obstetrics ", "جراحة النسائية والتوليد", "1"],
            ["Ophthalmologist", "جراحة و أمراض العيون", "1"],
            ["Children's Ophthalmology", "عيون أطفال", "1"],
            ["Allergy and Immunology", "الحساسية والمناعة", "1"],
            ["Pediatrics", "أمراض الأطفال", "1"],
            ["Dermatology and Venereology", "الامراض الجلدية والتناسلية", "1"],
            ["Pediatric Cardiology", "قلب أطفال", "1"],
            ["Pediatric Nephrology", "كلى أطفال ", "1"],
            ["Pediatric Endocrinologist", "غدد صم وسكري أطفال", "1"],
            ["Pediatric Gastroenterologist", "جهاز هضمي وكبد أطفال ", "1"],
            ["Pediatric Pulmonologist", "صدرية وحساسية أطفال", "1"],
            ["Pediatric Surgery", "جراحة أطفال", "1"],
            ["Pediatric Dentist", "أسنان أطفال", "2"],
            ["Pediatric Neurology", "دماغ وأعصاب أطفال", "1"],
            ["Pediatric Hematology and Oncology", "أمراض دم وأورام أطفال", "1"],
            ["Pediatrics and neonatology", "خداج أطفال", "1"],
            ["Cardiac surgery", "أختصاصي جراحة القلب والشرايين  ", "1"],
            ["General dentist", "طب الأسنان", "2"],
            ["Dentist surgery", "جراحة اسنان", "2"],
            ["Pain management", "اخصائي علاج الألم ", "1"],
        ];

        foreach ($categories as $category) {
            MedicalProviderCategory::create([
                'name' => $category[0],
                'name_ar' => $category[1],
                'parent' => (int)$category[2],
            ]);
        }
    }
}
