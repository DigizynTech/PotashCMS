<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToAdvanceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advance_requests', function (Blueprint $table) {
            $table->date('date')->nullable()->after('iban');
            $table->foreignId('created_by')->nullable()->after('iban');
            $table->foreignId(('last_modified_by'))->nullable()->after('iban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advance_requests', function (Blueprint $table) {
            //
        });
    }
}
