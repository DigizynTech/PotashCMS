<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title',255)->default('New Article');
            $table->text('content')->nullable();
            $table->enum('status', ['published', 'private'])->default('private');
            $table->enum('language', ['arabic','english']);
            $table->unsignedBigInteger('translationOfId')->default(0);
            $table->foreignId('created_by');
            $table->foreignId('last_updated_by');
            $table->boolean('sent')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
