<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_providers', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('name_ar',255)->nullable();
            $table->text('address');
            $table->text('address_ar')->nullable();
            $table->string('phone')->default('00000');
            $table->string('phone_2')->nullable();
            $table->foreignId('category')->default(0);
            $table->string('city')->default('Amman');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_providers');
    }
}
