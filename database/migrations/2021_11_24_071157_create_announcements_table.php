<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->string('title',255);
            $table->text('brief')->nullable();
            $table->dateTime('published_at');
            $table->text('content')->nullable();
            $table->foreignId('departmentId')->default(0);
            $table->foreignId('employeesId')->default(0);
            $table->enum('status', ['published', 'private'])->default('private');
            $table->enum('language', ['arabic','english']);
            $table->unsignedBigInteger('translationOfId')->default(0);
            $table->foreignId('created_by');
            $table->foreignId('last_updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
